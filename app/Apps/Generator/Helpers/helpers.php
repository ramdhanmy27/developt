<?php

function print_val($value)
// function getByType($value)
{
    switch (gettype($value)) {
        case "boolean":
            return $value ? "true" : "false";

        default:
        case "integer":
        case "double":
            return (string) $value;

        case "string":
            return '"'.$value.'"';
            break;
    }
}

function snc($input) 
{
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];

    foreach ($ret as &$match) {
        $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    
    return implode('_', $ret);
}

/**
 * Create archive file from files & folders
 * @param  string|array $path path to compress
 * @return string zip filepath
 */
function make_archive($target, $filename = null) 
{
    set_time_limit(0);
    
    $dest = $filename != null ? dirname($target)."/".$filename : rtrim($target, "[\/\\\\]").".zip";

    // prepare archive file
    $zip = new \ZipArchive();

    if (!$zip->open($dest, \ZipArchive::CREATE | \ZipArchive::OVERWRITE))
        throw new \Exception("Can't make archive file.");

    // archiving files
    foreach (rglob("$target/*.*") as $file) {
        $zip->addFile($file, substr($file, strpos($file, $target)+strlen($target)+1));
    }

    $zip->close();

    return $dest;
}

function download($target, $download_name = null, $delete_target = false) 
{
    // make as archive
    if (is_dir($target)) {
        $target = make_archive($target);
    }

    // handle download request
    $chunk_size = 1024*1024;
    $download_name = empty($download_name) ? basename($target) : $download_name;

    header("Content-Length: ".filesize($target));
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Content-Description: File Transfer");
    header("Content-Type: application/octet-stream");
    header("Content-Transfer-Encoding: binary");
    header("Expires: 0");
    header("Cache-Control: must-revalidate");
    header("Content-Disposition: attachment; filename=$download_name");

    $fopen = fopen($target, "r");

    while (!feof($fopen)) {
        print(fread($fopen, $chunk_size));
        ob_flush();
        flush();
    }

    fclose($fopen);

    if ($delete_target)
        @unlink($target);
}