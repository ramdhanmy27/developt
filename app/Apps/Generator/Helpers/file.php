<?php

function rglob($pattern, $flags = 0, $callback = null)
{
    $files = glob($pattern, $flags);

    if (is_callable($callback)) {
        $callback(dirname($pattern), $files);
    }

    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
    }

    return $files;
}

function make_dir($path, $split = "/") 
{
    $tmp = "";

    foreach (explode($split, $path) as $dir) {
        $tmp .= $dir.'/';

        if (!file_exists($tmp) && is_writable(dirname($tmp))) {
            mkdir($tmp);
        }
    }

    return $tmp;
}

function rm_dir($path)
{
    foreach (glob($path."/*") as $file) {
        if (is_dir($file)) {
            rm_dir($file);
        }
        else @unlink($file);
    }

    @rmdir($path);
}

function tmp_path($path = null)
{
    return config("generator.path.tmp").($path === null ? "" : "/".$path);
}

function template_path($path = null)
{
    return config("generator.path.template").($path === null ? "" : "/".$path);
}

function root_path($path = null) 
{
    return $_SERVER["DOCUMENT_ROOT"].($path === null ? "" : "/".$path);
}