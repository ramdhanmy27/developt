<?php

use App\Apps\Generator\Middleware\GeneratorMiddleware;
use App\Apps\Generator\Repositories\Template;

// Generators
Route::group(["namespace" => "Generator", "middleware" => GeneratorMiddleware::class], function() {
    // Code
    Route::menu(["title" => "Code Generator", "icon" => "fa fa-code text-success"], function() {
        // Laravel
        Route::menu(["title" => "Laravel", "icon" => "fa fa-laravel"], function() {
            Route::controller("laravel", "LaravelController")
                ->menu("CRUD", "/", "fa fa-codepen")
                ->menu("Model", "model", "fa fa-database")
                ->menu("Migration", "migration", "fa fa-exchange");
        });
    });

    // Database
    Route::menu(["title" => "Database Generator", "icon" => "fa fa-database text-info"], function() {
        // PostgreSQL
        Route::controller("pgsql", "PgsqlController")
            ->menu("PostgreSQL", "/", "fa fa-pgsql");
    });
});

// Settings
Route::menu(["title" => "Settings", "icon" => "fa fa-cog text-muted", "namespace" => "Settings"], function() {
    Route::menu(["title" => "Template", "icon" => "fa fa-file-code-o"], function() {
        Route::controller("template/{base}", "TemplateBaseController");

        foreach (Template::instance()->bases() as $base) {
            Route::menu([
                "title" => title_case($base), 
                "icon" => "fa fa-$base",
            ], function() use ($base) {
                foreach (Template::instance()->sections($base) as $section) {
                    $section = basename($section);
                    
                    Route::menu([
                        "title" => title_case($section), 
                        "url" => "editor/$base/$section", 
                        "icon" => "fa fa-$section",
                    ]);
                }
            });
        }
    });

    Route::controller("connection", "ConnectionController")
        ->menu("Connection", "/", "fa fa-retweet");
});

/** Services */
Route::group(["prefix" => "service"], function() {
    Route::controller("data", "DataController");
});