@extends("app")

@section("title", title_case($base))

@section("content")
	<div class="row">
		@foreach ($section as $item)
			<?php $item = basename($item); ?>

			<a href="{{ url("editor/$base/$item") }}" 
				class="panel panel-default panel-heading col-md-6">
				<b>{{ title_case(basename($item)) }}</b>
			</a>
		@endforeach
	</div>
@endsection