@extends("app")

@section("title", trans("action.create")." | Connection")

@section("content")
    @include("settings.connection.form-$model->driver", [
        "model" => $model,
    ])
@endsection
