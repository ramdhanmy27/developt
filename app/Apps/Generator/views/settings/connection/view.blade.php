@extends("app")

@section("title", "$model->name | Connection")

@section("content")
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="row mt-none mb-none">
				<div class="col-md-6 pt-sm"> {{ $model->name }} </div>
				<div class="col-md-6 text-right">
					{{ config("generator.connection.drivers.$model->driver.name") }}
					<img src="{{ asset("img/icon/$model->driver-big.png") }}" height="60px" />
				</div>
			</h1>
		</div>

		<div class="panel-body">
			<div class="row">
				{!! Html::viewLabel($model, "name") !!}
				{!! Html::viewLabel($model, "driver") !!}
				{!! Html::viewLabel($model, "username") !!}
				{!! Html::viewLabel($model, "prefix") !!}
				{!! Html::viewLabel($model, "password") !!}

				@foreach ($model->config as $label => $value)
					@include("ui.view-label", [
						"field" => id2title($label, "[-_]"), 
						"value" => $value,
					])
				@endforeach
			</div>
		</div>

		@if (!Request::ajax())
			<div class="panel-footer row">
				<div class="col-md-6">
					<a href="{{ url("generator/connection") }}" class="btn btn-default pr-md">
						<i class="fa fa-reply"></i> @lang("action.back")
					</a>
				</div>
				
				<div class="col-md-6 text-right">
					<a href="{{ url("generator/connection/edit/$model->id") }}" class="btn btn-warning">
						<i class="fa fa-edit"></i> @lang("action.update")
					</a>
					<a href="{{ url("generator/connection/delete/$model->id") }}" 
	          class="btn btn-danger" confirm="@lang("confirm.delete")">
	          <i class="fa fa-trash"></i> @lang("action.delete")
	        </a>
				</div>
			</div>
		@endif
	</div>
@endsection