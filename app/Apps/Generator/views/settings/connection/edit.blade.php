@extends("app")

@section("title", trans("action.update")." | Connection")

@section("content")
    @include("settings.connection.form-$model->driver", [
    	"model" => $model,
    ])
@endsection
