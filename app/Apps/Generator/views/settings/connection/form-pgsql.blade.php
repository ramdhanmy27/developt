<?php

use App\Apps\Generator\Models\Connection;

?>

{!! Form::model($model) !!}
    {!! Form::hidden("driver", $model->driver) !!}
    {!! Form::group("text", "name") !!}
    {!! Form::group("text", "host", null, $model->config->host) !!}
    {!! Form::group("text", "port", null, $model->config->port) !!}
    {!! Form::group("text", "username") !!}
    {!! Form::group("text", "password") !!}

    <div class="form-group">
        <div class="col-md-offset-3 col-md-6">
            <button type="submit" class="btn btn-primary"> @lang("action.save") </button>
            <button type="submit" class="btn btn-default"> Test Connection </button>
        </div>
    </div>
{!! Form::close() !!}