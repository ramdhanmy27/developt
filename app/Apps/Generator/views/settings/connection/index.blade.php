<?php

use App\Apps\Generator\Models\Connection;

?>

@extends("app")

@section("title", "Connection")

@section("content")
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-plus"></i> @lang("action.create") <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            @foreach (config("generator.connection.drivers") as $driver => $item)
                <li> 
                    <a href="{!! url("generator/connection/add?driver=$driver") !!}">
                        {{ $item["name"] }}
                    </a> 
                </li>
            @endforeach
        </ul>
    </div>
    <hr>

    <table class="table table-bordered" datatable="{!! url("generator/connection/data") !!}">
        <thead>
            <tr>
                <th dt-col="#driver" width="20px"> </th>
                <th dt-col="#name"> Name </th>
                <th dt-col="#dt-action" sort="false" search="false" width="100px"> </th>
            </tr>
        </thead>
        
	    <dt-template>
            <div id="driver">
                <div class="text-center">
                    <i class="fa fa-[[driver]]"></i>
                </div>
            </div>
            <div id="name"> [[name]] </div>
	        <div id="dt-action">
                <a href="{{ url("generator/connection/view/[[id]]") }}" modal
                    tooltip="@lang("action.view")" class="btn btn-sm btn-info">
                    <i class="fa fa-eye"></i>
                </a>
	            <a href="{{ url("generator/connection/edit/[[id]]") }}" modal
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("generator/connection/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
