{!! Form::group("file", "uml[file]", "UML File") !!}

<div class="form-group">
    <div class="col-md-offset-3 col-md-6">
        <button name="generate" type="submit" class="btn btn-primary" value="uml"> 
            <i class="fa fa-gears"></i> Generate
        </button>
    </div>
</div>
