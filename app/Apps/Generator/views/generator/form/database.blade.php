<?php

use App\Apps\Generator\Models\Connection;

$conn_opt = Connection::orderBy("driver")->orderBy("name")
    ->get()->keyBy("id")
    ->map(function($item, $i) {
        return $item->name." <b>(".config("generator.connection.drivers.$item->driver.name").")</b>";
    });

?>

{!! Form::group("select", "database[connection]", "Connection", $conn_opt) !!}
{!! Form::group("select", "database[database]", "Database", [], null, [
    "ref-select" => "database[connection]|database",
]) !!}
{!! Form::group("select", "database[schema]", "Schema", [], null, [
    "ref-select" => "database[database]|schema", 
    "ref-values" => "connection:database[connection]",
]) !!}

<div class="form-group">
    <label for="table" class="col-md-3 control-label">Table</label>
    <div class="col-md-6">
        {!! Form::select("database[table][]", [], null, [
            "class" => "tables-selection",
            "ref-select" => "database[schema]|table", 
            "ref-values" => "connection:database[connection],database:database[database]",
            "multiple" => "",
        ]) !!}
        <div class="row mt-sm">
            <div class="col-md-6">
                <button type="button" class="btn btn-sm btn-primary select-tables">
                    <i class="fa fa-mouse-pointer"></i> Select All
                </button>
                <button type="button" class="btn btn-sm btn-default clear-tables">
                    <i class="fa fa-trash"></i> Clear
                </button>
            </div>
            <p class="mb-none description col-md-6">
                *Jika dikosongkan aplikasi akan mengenerate untuk setiap table.
            </p>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-6">
        <button name="generate" type="submit" class="btn btn-primary" value="database"> 
            <i class="fa fa-gears"></i> Generate
        </button>
        <button name="debug" type="submit" class="btn btn-warning" value="database"> 
            <i class="fa fa-cog"></i> Debug
        </button>
        {{-- <button name="save" type="submit" class="btn btn-success" value="database"> 
            <i class="fa fa-floppy-o"></i> Save
        </button> --}}
    </div>
</div>

@push("app-script")
<script type="text/javascript">
    $(document).ready(function() {
        var $target = $(".tables-selection");

        $(".select-tables").click(function() {
            var values = [];

            $target.find("option").each(function() {
                values.push(this.value);
            })

            $target.val(values);
            $target.trigger("change");
        })

        $(".clear-tables").click(function() {
            $target.val("").trigger("change");
        })
    })
</script>
@endpush