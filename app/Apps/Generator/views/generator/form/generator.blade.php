@section("input", true)

<?php

$tab_detail = [
    "database" => [
        "label" => "Database",
        "icon" => "fa fa-database",
    ],
    "uml" => [
        "label" => "UML",
        "icon" => "fa fa-object-group",
    ],
];

?>

{!! Form::model(@$model, ["ref-url" => url("generator/service/data"), "files" => true]) !!}
    {{ Form::rules([
        "template" => "required",
    ]) }}


    <div class="panel panel-primary">
        <h4 class="panel-heading mb-none"> 
            <i class="fa fa-code"></i> Code Generator 
        </h4>

        <div class="panel-body"> 
            @yield("form") 

            {!! Form::group("select", "template", null, $templates, $templates->last()) !!}

            <div class="form-group">
                <label for="uml" class="col-md-3 control-label"> Export Path </label>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">{{ root_path() }}/</span>
                        <input class="form-control" name="export_path" type="text">
                    </div>
                    <p class="help-block text-muted">
                        *Jika dikosongkan file akan didownload
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="tabs">
        <ul class="nav nav-tabs">
            @foreach ($tabs as $i => $tab)
                <li @if ($i == 0) class="active" @endif>
                    <a href="#{{ $tab }}" data-toggle="tab"> 
                        <i class="{{ $tab_detail[$tab]["icon"] }}"></i> 
                        {{ $tab_detail[$tab]["label"] }}
                    </a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content">
            @foreach ($tabs as $i => $tab)
                <div id="{{ $tab }}" class="tab-pane @if ($i == 0) active @endif">
                    @include("generator.form.$tab")
                </div>
            @endforeach
        </div>
    </div>

{!! Form::close() !!}