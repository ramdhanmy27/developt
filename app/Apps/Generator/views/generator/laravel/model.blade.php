@extends("app")

@section("title", "Model | Laravel")
@section("form-title", "Settings")

@section("form")
    {!! Form::group("text", "namespace", "Namespace", @$model->value->namespace) !!}
@endsection

@section("content")
    @include("generator.form.generator", ["tabs" => ["database", "uml"]])
@endsection
