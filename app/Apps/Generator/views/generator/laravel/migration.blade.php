@extends("app")

@section("title", "Model | Laravel")
@section("form-title", "Settings")

@section("content")
    @include("generator.form.generator", ["tabs" => ["database", "uml"]])
@endsection
