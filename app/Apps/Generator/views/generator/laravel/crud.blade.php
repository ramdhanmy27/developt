@extends("app")

@section("title", "CRUD | Laravel")
@section("form-title", "Settings")

@section("form")
    {!! Form::group("text", "namespace", "Namespace", @$model->value->namespace) !!}
    {!! Form::group("text", "model_namespace", "Model Namespace", @$model->value->model_namespace) !!}
    {!! Form::group("text", "base_url", "Base URL", @$model->value->base_url) !!}
@endsection

@section("content")
    @include("generator.form.generator", ["tabs" => ["database", "uml"]])
@endsection
