@extends("app")

@section("title", "Database | PostgreSQL")
@section("input", true)

@section("content")
    <div class="tabs">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#uml" data-toggle="tab"> <i class="fa fa-object-group"></i> UML </a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="uml" class="tab-pane active">
                @include("generator.form.uml")
            </div>
        </div>
    </div>
@endsection
