<?php

namespace App\Apps\Generator\Controllers;

use App\Apps\Generator\Models\Connection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller 
{
    public function postIndex(Request $req) 
    {
        $method = "command".ucfirst($req->command);

        if (method_exists($this, $method))
            return response()->json($this->{$method}($req));
        else
            return response()->json($this->fetch($req));
    }

    protected function fetch($req)
    {
        // return response()->json(
        //     collect(
        //         DB::table($table)->where($req->filter, $req->value)->get()
        //     )
        //     ->map(function($item) use ($columns) {
        //         return ["text" => $item->{$columns[0]}, "id" => $item->{$columns[1]}];
        //     })
        // );
    }

    protected function commandDatabase($req)
    {
        $driver = $this->getModelConn($req->value)->getDriver();

        return collect($driver->databases())
            ->map(function($item) {
                return ["text" => $item->name, "id" => $item->name];
            });
    }

    protected function commandSchema($req)
    {
        $driver = $this->getModelConn($req->connection)->getDriver();

        if (!method_exists($driver, "schemas"))
            return;

        return collect($driver->schemas($req->value))
            ->map(function($item) {
                return ["text" => $item->name, "id" => $item->name];
            });
    }

    protected function commandTable($req)
    {
        $driver = $this->getModelConn($req->connection)->getDriver();

        return collect($driver->tables([$req->value, $req->database]))
            ->map(function($item) {
                return ["text" => $item->name, "id" => $item->name];
            });
    }

    private function getModelConn($id)
    {
        $conn = Connection::find($id);

        if ($conn == null)
            abort(500, "Connection failed");

        return $conn;
    }
}