<?php

namespace App\Apps\Generator\Controllers\Settings;

use App\Apps\Generator\Models\Connection;
use App\Generators\Laravel\ModelGenerator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

/**
 * Need database to run
 */
class ConnectionController extends Controller
{
    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(Connection::select("*"))->make(true);
    }

    public function getAdd(Request $req)
    {
        // try {
            return view([
                "model" => new Connection(["driver" => $req->driver]),
            ]);
        // }
        // catch(\Exception $e) {
        //     abort(404);
        // }
    }

    public function postAdd(Request $req)
    {
        $model = Connection::createOrFail(input_filter($req->all()));

        return redirect("generator/connection/view/$model->id");
    }

    public function getEdit($id)
    {
        return view([
            "model" => Connection::findOrFail($id),
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Connection::findOrFail($id)->updateOrFail(input_filter($req->all()));

        return redirect("generator/connection/view/$id");
    }

    public function getView($id)
    {
        return view([
            "model" => Connection::findOrFail($id),
        ]);
    }

    public function getDelete($id)
    {
        Connection::findOrFail($id)->delete();
        
        return redirect("generator/connection");
    }

    public function postTest(Request $req)
    {
        if (!Connection::fill(input_filter($req->all()))->test())
            abort(500);
    }
}
