<?php

namespace App\Apps\Generator\Controllers\Settings;

use App\Apps\Generator\Repositories\Template;
use App\Generators\Laravel\ModelGenerator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TemplateBaseController extends Controller
{
    public function getIndex($base)
    {
        $section = Template::instance()->sections($base);

        return view(compact("base", "section"));
    }

    public function postAdd(Request $req)
    {
        // code goes here..
    }

    public function postEdit(Request $req)
    {
        // code goes here..
    }

    public function postDelete(Request $req)
    {
        // code goes here..
    }
}
