<?php

namespace App\Apps\Generator\Controllers\Traits;

use App\Apps\Generator\Support\Parser\DatabaseParser;
use App\Apps\Generator\Support\Parser\UMLParser;
use Illuminate\Http\Request;

trait GeneratorTrait
{
    private $debug = false;

    protected function generate($generator, $path = null)
    {
        set_time_limit(0);
        
        // export to certain directory
        if (!empty($path)) {
            $generator->generate(root_path($path));

            if ($this->debug)
                dd("Direct generated", "Destination: ".$path);

            \Flash::success("Code Generated successfully. <br> <i>".root_path($path)."</i>");

            return back();
        }
        // download as arhive file
        else {
            $path = tmp_path("download/".uniqid());

            // download & remove archive
            download($generator->generate($path), "code-".uniqid().".zip", true);

            if ($this->debug)
                dd("Download as archive", "Destination: ".$path);

            // remove temporary
            rm_dir($path);
        }
    }

    /**
     * Make parser object from request data
     * @param  Request $req
     * @return Parser
     */
    protected function parse(Request $req)
    {
        if ($req->has("debug")) {
            $this->debug = true;
            $parser = $req->debug;
        }
        else $parser = $req->generate;

        $method = "parseFrom".id2camel($parser, "[-_]");

        if (!method_exists($this, $method))
            abort(500, "Cannot parse input for '$parser'.");

        return $this->{$method}($req);
    }

    private function parseFromDatabase($req)
    {
        return new DatabaseParser($req->database);
    }

    private function parseFromUml($req)
    {
        $file = $req->uml["file"];

        // set download filename
        $filename = $file->getClientOriginalName();
        $this->download_name = pathinfo($filename, PATHINFO_FILENAME);

        // upload file
        $filename = uniqid()."-".$this->download_name;
        $file->move(tmp_path(UMLParser::NAME), $filename);

        // create parser
        $parser = new UMLParser($filename);

        // remove uploaded file
        @unlink($parser->filepath);

        return $parser;
    }
}
