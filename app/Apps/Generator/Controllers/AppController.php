<?php

namespace App\Apps\Generator\Controllers;

use App\Generator\SQLGenerator;
use App\Parser\SQLParser;
use App\Parser\UMLParser;
use Alchemy\Zippy\Zippy;

class AppController extends Controller
{
    public function index() 
    {
        return view("app", [
            "data" => "Adsfsdf"
        ]);
    }

    public function _index() 
    {
    	// build generator
        $parser = new UMLParser(ROOT_DIR."/storage/app/public/ERPModel.mdj");
        $generator = new SQLGenerator($parser->getObjectData());

        return $generator->download("file-download.zip"); // download as archive file
    }

    /**
     * Make download response for generated 
     * @return Response
     */
    public function download($name = "generated-sql.zip") 
    {
        $chunk_size = 1024*1024;

        header("Content-Length: ".filesize($file));
        header("Content-Description: File Transfer");
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: binary");
        header("Expires: 0");
        header("Cache-Control: must-revalidate");
        header("Content-Disposition: attachment; filename=".(empty($downloadName) ? basename($file) : $downloadName));

        $this->makeArchive("tmp/".$name);

        while (!feof($fopen)) {
            print(fread($fopen, $chunk_size));
            ob_flush();
            flush();
        }

        @unlink($file);
    }

    /**
     * Create archive file from files & folders
     * @param  string|array $path path to compress
     * @return string zip filepath
     */
    public function makeArchive($path) 
    {
        set_time_limit(0);

        $filepath = "tmp/".date("Ymd-his").".zip";
        Zippy::load()->create($filepath, is_array($path) ? $path : [$path]);

        return $filepath;
    }
}
