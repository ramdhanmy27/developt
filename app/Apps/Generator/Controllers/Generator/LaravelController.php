<?php

namespace App\Apps\Generator\Controllers\Generator;

use App\Apps\Generator\Controllers\Traits\GeneratorTrait;
use App\Apps\Generator\Models\ParserTemplate;
use App\Apps\Generator\Repositories\Template;
use App\Apps\Generator\Support\Generator\Laravel\CrudGenerator;
use App\Apps\Generator\Support\Generator\Laravel\MigrationGenerator;
use App\Apps\Generator\Support\Generator\Laravel\ModelGenerator;
use App\Apps\Generator\Support\Generator\SQLGenerator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LaravelController extends Controller
{
    const BASE = "laravel";

    use GeneratorTrait;

    private $tpl;

    public function __construct()
    {
        $this->tpl = Template::instance();
    }

    /**
     * CRUD Generator
     * @return View
     */
    public function getIndex()
    {
        $model = new ParserTemplate();
        $model->value = [
            "namespace" => "App\\Http\\Controllers",
            "model_namespace" => "App\\Models",
        ];

        $templates = $this->getTemplates("crud");

        return view("generator.laravel.crud", compact("model", "templates"));
    }

    public function postIndex(Request $req)
    {
        $generator = new CrudGenerator(
            $this->parse($req)->getObjectData(), 
            $req->template
        );

        $generator->set($req->input());

        return $this->generate($generator, $req->export_path);
    }

    /**
     * Model Generator
     * @return View
     */
    public function getModel()
    {
        $model = new ParserTemplate();
        $model->value = [
            "namespace" => "App\\Models",
        ];

        $templates = $this->getTemplates("model");

        return view("generator.laravel.model", compact("model", "templates"));
    }

    public function postModel(Request $req)
    {
        $generator = new ModelGenerator(
            $this->parse($req)->getObjectData(), 
            $req->template
        );

        return $this->generate($generator, $req->export_path);
    }

    /**
     * Migration Generator
     * @return View
     */
    public function getMigration()
    {
        $templates = $this->getTemplates("migration");

        return view("generator.laravel.migration", compact("templates"));
    }

    public function postMigration(Request $req)
    {
        $generator = new MigrationGenerator(
            $this->parse($req)->getObjectData(), 
            $req->template
        );

        return $this->generate($generator, $req->export_path);
    }

    private function getTemplates($section)
    {
        return $this->tpl->projects(self::BASE, $section)->all()
            ->keyBy(function($item) { return $item; });
    }
}
