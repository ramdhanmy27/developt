<?php

namespace App\Apps\Generator\Controllers\Generator;

use App\Apps\Generator\Support\Generator\ModelGenerator;
use App\Apps\Generator\Support\Generator\SQLGenerator;
use App\Apps\Generator\Support\Parser\UMLParser;
use Illuminate\Http\Request;

class PgsqlController extends Controller
{
    /**
     * SQL Generator
     * @return View
     */
    public function getIndex()
    {
        return view("generator.pgsql.index");
    }

    public function postIndex(Request $req)
    {
        $original_name = $req->uml->getClientOriginalName();
        $filename = uniqid()."-".$original_name;
        $req->uml->move(tmp_path("uml"), $filename);

        $parser = new UMLParser($filename);
        $generator = new SQLGenerator($parser->getObjectData());

        download(
            $generator->generate(tmp_path("download")), 
            pathinfo($original_name, PATHINFO_FILENAME).".sql"
        );
    }
}
