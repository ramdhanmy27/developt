<?php

namespace App\Apps\Generator\Middleware;

class GeneratorMiddleware
{
    public function handle($request, $next)
    {
        if ($request->has("save")) {
            // save data as parser template

            \Flash::success("Konfigurasi anda telah disimpan.");

            return back();
        }

        return $next($request);
    }
}