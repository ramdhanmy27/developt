<?php

namespace App\Apps\Generator\Repositories;

use App\Apps\Editor\Repositories\Project;
use App\Services\Traits\Singleton;
use Illuminate\Support\Facades\Storage;

class Template
{
	use Singleton;

	private $disk;

	public $root;

	public function __construct()
	{
		$this->root = config("editor.path");
		
		$this->disk = Storage::createLocalDriver([
			"root" => $this->root,
		]);
	}

	public function __call($method, $args)
	{
		return call_user_func_array([$this->disk, $method], $args);
	}

	public function bases()
	{
		return $this->disk->directories("/");
	}

	public function sections($base)
	{
		return $this->disk->directories("$base");
	}

	public function projects($base, $section)
	{
		return new Project($this->root."/".$base."/".$section);
	}
}