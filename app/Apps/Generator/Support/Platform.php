<?php

namespace App\Apps\Generator\Support;

class Platform 
{
	use Traits\ObjectClass;

	/**
	 * Platform name <<required>>
	 * @var String
	 */
	private $id;

	/**
	 * Image logo url
	 * @var String
	 */
	private $logo = "default.png";

	/**
	 * Platform Label 
	 * @var String
	 */
	private $name;

	/**
	 * fillable property by array
	 * @var array
	 */
	protected $fillable = ["name", "logo"];

	public function __construct($id, $attr = null) 
	{
		$this->id = $id;

		if (is_array($attr))
			$this->fillByArray($attr, [
				"name" => id2title($this->id),
			]);
	}
}