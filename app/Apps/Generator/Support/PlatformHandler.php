<?php

namespace App\Apps\Generator\Support;

use App\Apps\Generator\Support\Platform;

class PlatformHandler 
{
	private $platforms = [];

	public function __construct() 
	{
		$this->addPlatformByConfig();
	}

	/**
	 * Add platform from config array
	 */
	public function addPlatformByConfig() 
	{
		foreach (config("app.platform") as $id => $attr) {
			$this->add(new Platform($id, $attr));
		}
	}

	/**
	 * Add new platform into collections
	 * @param Platform $platform [description]
	 */
	public function add(Platform $platform) 
	{
		$this->platforms[$platform->id] = $platform;
	}

	/**
	 * Retrieve all platform collections
	 * @return array[Platform]
	 */
	public function all() 
	{
		return $this->platforms;
	}

	/**
	 * Get Platform instance by ID
	 * @param  String $id
	 * @return 
	 */
	public function get($id) 
	{
		if (!isset($this->platforms[$id]))
			return null;

		return $this->platforms[$id];
	}

	/**
	 * Get Platform instance from current accessed url
	 * @return Platform
	 */
	public function getFromUrl() 
	{
		return $this->get(app("request")->segment(2));
	}
}