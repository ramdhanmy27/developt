<?php

namespace App\Apps\Generator\Support\Data\Database;

class Field 
{
	/**
	 * Table Name
	 * @var String
	 */
	public $name;

	public $type;

	public $isNull = true;

	public $isPrimary = false;

	public $isUnique = false;

	public $isIncrementing = false;

	public $dimension = 0;

	public $length = 0;

	public $defaultValue;

	public function __construct($name, $type) 
	{
		$this->name = strtolower($name);
		$this->setType(strtolower($type));
	}

	public function setType($type)
	{
		preg_match_all("/(\w+?)\((\d+?)\)/", $type, $match);
		$this->type = isset($match[1][0]) ? $match[1][0] : $type;
		$this->length = isset($match[2][0]) ? $match[2][0] : $this->defaultLength($type);
	}

	public function setPrimary()
	{
		$this->isNull = false;
		$this->isPrimary = true;
		$this->isUnique = true;
		$this->isIncrementing = $this->type == "int" || $this->type == "integer";
	}

	public function defaultLength($type)
	{
		switch ($type) {
			case 'varchar':
				return 255;
			
			default:
				return 0;
		}
	}
}