<?php

namespace App\Apps\Generator\Support\Data\Database;

class Relation 
{
	/**
	 * Table Name
	 * @var String
	 */
	public $name;

	public $type;

	public function __construct($name, $type) 
	{
		$this->name = $name;
		$this->type = $type;
	}
}