<?php

namespace App\Apps\Generator\Support\Data\Database;

use App\Apps\Generator\Support\Data\Database\Field;
use App\Apps\Generator\Support\Data\Database\Relation;

class Table 
{
	/**
	 * Table Name
	 * @var String
	 */
	public $name;
	
	/**
	 * Field list
	 * @var Field[]
	 */
	public $fields = [];

	/**
	 * Relation list
	 * @var Relation[]
	 */
	public $relations = [];

	/**
	 * Primary Key Field
	 * @var Field
	 */
	public $primaryKey;

	public function __construct($name = null, $fields = []) 
	{
		$this->name = $name;
		$this->fields = $fields;
	}

	public function addPrimary($name, $type)
	{
		$field = new Field($name, $type);
		$field->setPrimary();

		$this->primaryKey = $field;
		
		return $this->fields[] = $field;
	}

	public function addField($name, $type)
	{
		return $this->fields[] = new Field($name, $type);
	}

	public function addRelations($name, $type)
	{
		return $this->relations[] = new Relation($name, $type);
	}
}