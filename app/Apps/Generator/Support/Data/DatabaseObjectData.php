<?php

namespace App\Apps\Generator\Support\Data;

use App\Apps\Generator\Support\Data\Database\Table;

class DatabaseObjectData extends ObjectData 
{
	/**
	 * Table Lists
	 * @var array[Table]
	 */
	public $tables;

	public function addTable(Table $table) 
	{
		$this->tables[] = $table;
	}

	public function getTables() 
	{
		return $this->tables;
	}
}