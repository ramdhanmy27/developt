<?php

namespace App\Apps\Generator\Support\Parser;

use App\Apps\Generator\Support\Data\ObjectData;

interface ParserContract 
{
	const NAME = "parser";

	/**
	 * Create new Object Data instance
	 * @return ObjectData
	 */
	public function createObjectData();

	/**
	 * set ObjectData instance
	 * @return ObjectData
	 */
	public function setObjectData(ObjectData $object_data);

	/**
	 * return ObjectData instance
	 * @return ObjectData
	 */
	public function getObjectData();
}