<?php

namespace App\Apps\Generator\Support\Parser;

use App\Apps\Generator\Models\Connection;
use App\Apps\Generator\Support\Data\DatabaseObjectData;
use App\Apps\Generator\Support\Data\Database\Table;

class DatabaseParser extends Parser 
{
	/**
	 * Connection driver database
	 * @var DriverContract $driver
	 */
	public $driver;

	/**
	 * Connection config
	 * @var array
	 */
	private $connection = [];

	/**
	 * Array containing tables name
	 * if empty, use all tables
	 * @var array[string]
	 */
	private $tables = [];

	public function init(array $conn)
	{
		$this->driver = Connection::find($conn["connection"])->getDriver();
		$this->connection = array_only($conn, [
			"connection", "database", "schema",
		]);

		if (isset($conn["table"])) 
			$this->tables = $conn["table"];
	}

	/**
	 * @inheritDoc
	 */
	public function createObjectData()
	{
		$obj = new DatabaseObjectData;

		$tables = collect(
				$this->driver->fields($this->connection["database"], $this->tables)
			)->groupBy("table");

		foreach ($tables as $table_name => $fields) {
			$table = new Table($table_name);

			foreach ($fields as $field) {
				if ($field->is_primary) {
					$table->addPrimary($field->name, $field->type);
				}
				else {
					$tfield = $table->addField($field->name, $field->type);
					$tfield->isNull = $field->is_null;
					$tfield->defaultValue = $field->default;
					$tfield->isUnique = $field->is_unique;

					if ($tfield->length != null)
						$tfield->length = $field->length;
				}
			}

			// register table into object data
			$obj->addTable($table);
		} 

		$this->setObjectData($obj);
	}
}