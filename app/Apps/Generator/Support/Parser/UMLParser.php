<?php

namespace App\Apps\Generator\Support\Parser;

use App\Apps\Generator\Support\Data\DatabaseObjectData;
use App\Apps\Generator\Support\Data\Database\Table;
use App\Apps\Generator\Support\Parser\UML\UMLDesign;

class UMLParser extends Parser 
{
	const NAME = "uml";
	
	private $uml;

	public $filepath;

	public function init($file)
	{
		// Read uml file
		$this->uml = new UMLDesign;
		$this->filepath = tmp_path(self::NAME."/$file");
		$this->uml->ImportFromFile($this->filepath);
	}

	/**
	 * @inheritDoc
	 */
	public function createObjectData() 
	{
		$obj = new DatabaseObjectData;

		foreach ($this->uml->getClasses() as $class) {
			$table = new Table(snc($class->Name));

			$table->addPrimary(snc($class->PKName), snc($class->PKType));

			// Fields
			if (is_array($class->Attributes)) {
				foreach ($class->Attributes as $attr) {
					$table->addField(snc($attr->Name), $attr->Datatype);
				}
			}

			// Relations
			if (is_array($class->Relations)) {
				foreach ($class->Relations as $rel) {
					$table->addRelations(snc($rel->Name), $rel->LinkedClass->FKType);
				}
			}

			// register table into object data
			$obj->addTable($table);
		}

		// Register object data
		$this->setObjectData($obj);

		return true;
	}
}