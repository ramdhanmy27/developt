<?php

namespace App\Apps\Generator\Support\Parser\UML;

class UMLDesign 
{
	public $ppackages = [];
	public $pclasses = [];

	public function getPackages() 
	{
		return $this->ppackages;
	}

	public function getClasses() 
	{
		return $this->pclasses;
	}

	private function SearchForPackage($astream) 
	{
		$stype = $astream['_type']; 
	    $uid = $astream['_id'];
	  	
	  	if ($stype=='UMLPackage') {
	  		$this->ppackages[$uid] = $astream['name'];
	  	}
	  	elseif (isset($astream['ownedElements'])) {
	  		foreach ($astream['ownedElements'] as $elm)
		      	$this->SearchForPackage($elm);
	   }
	}

	public function FindClassByID($aid) 
	{
		foreach ($this->pclasses as $cls) {
			if ($cls->cls_id==$aid)
				return $cls;
		}

		return false;
	}

	public function FindClassByName($aclsname) 
	{
		foreach ($this->pclasses as $cls) {
			if (strtolower($aclsname) == strtolower($cls->Name))
				return $cls;
		}

		return false;
	}
	
	private function SearchForClass($astream) 
	{
		$stype = $astream['_type']; 
	    $uid = $astream['_id'];
	  	
	  	if ($stype=='UMLClass') {
	  		$cls = new UMLClass($this);
	  		$cls->Name = $astream['name'];
	  		$cls->cls_id = $uid;
	  		$cls->prnt_id = $astream['_parent']['$ref'];
	  		$isPKdefined = false;
	  		$isFKdefined = false;

	  		if (isset($astream["attributes"])) {
		  		foreach ($astream["attributes"] as $attr) {
		  			$sname = $attr['name'];
		  			$stype = isset($attr['type']) ? $attr['type'] : "varchar";
		  			
		  			if (isset($attr["stereotype"])) {
		  				$strtype = $attr['stereotype'];
		  				
		  				if ($strtype=='pkey') {
		  					$isPKdefined = true;
		  					$cls->PKName = $sname;
		  					$cls->PKType = $stype;
		  				}
		  				elseif ($strtype=='fkey') {
		  					$isFKdefined = true;
		  					$cls->FKName = $sname;
		  					$cls->FKType = $stype;
		  				}
		  			}
		  			else {
		  				$atrdec = new Attribute;
				  		$atrdec->Name = $sname;
				  		$atrdec->Datatype = $stype;
				  		$cls->Attributes[] = $atrdec;
		  			}
		  		}
	  		}
	  		
	  		if (!$isPKdefined) {
	  			$cls->PKName = 'id';
	  			// $cls->PKName = $cls->Name.'_id';
	  			$cls->PKType = 'Int';
	  		}
	  		
	  		if (!$isFKdefined) {
	  			$cls->FKName = $cls->PKName;
	  			$cls->FKType = $cls->PKType;
	  		}

	  		if (isset($astream['ownedElements'])) {
		  		foreach ($astream['ownedElements'] as $rel) {
		  			$ed2 = $rel['end2'];
		  			$ref = $ed2['reference'];

		  			$reldec = new Relation($this);
		  			$reldec->Linkedid = $ref['$ref'];
		  			
		  			if (isset($rel['stereotype']) && $rel['stereotype']=='optional') {
	  					$reldec->isOptional = true;
		  			}

		  			$cls->Relations[] = $reldec;
		  		}
	  		}

	  		$this->pclasses[] = $cls;
	  	} 
	  	elseif (isset($astream['ownedElements'])) {
	  		foreach ($astream['ownedElements'] as $elm)
		      	$this->SearchForClass($elm);
	    }
	}

	public function ImportFromFile($filename) 
	{
		$raw = json_decode(file_get_contents($filename, true), true);

		$this->SearchForPackage($raw, $this->ppackages);
		$this->SearchForClass($raw, $this->pclasses);
		//rebuild references

		foreach ($this->pclasses as $cls) {
			$cls->LoadRelations();
			$cls->ResolvePkgname();
		}
	}		
}