<?php

namespace App\Apps\Generator\Support\Parser\UML;

class UMLClass 
{
	private $Owner;
	public $Name; 
	public $cls_id; 
	public $prnt_id; // temporary variable
	public $PkgName;
	public $PKName; // didefinisikan ketika loading. jika tidak ada atribut yg ditandai sbg PK, maka otomatis dibuat PK dg nama Name_ID bertipe integer
	public $PKType;
	public $FKName; // didefinisikan ketika loading. jika tidak ada atribut yg ditandai sbg FK, maka FK sekaligus PK
	public $FKType;
	public $Attributes = [];
	public $Relations = [];
	public $Methods;
	public $List;

	public function __construct($aowner) 
	{
		$this->Owner = $aowner;
	}

	public function LoadRelations() 
	{
		foreach ($this->Relations as $rel)
			$rel->ResolveLink();
	}
	
	public function ResolvePkgname() 
	{
		$this->PkgName = $this->Owner->ppackages[$this->prnt_id];
	}
	
	public function FindAttribute($attrname) 
	{
		foreach ($this->Attributes as $attr) {
			if (strtoupper($attrname) == strtoupper($attr->Name))
				return $attr;
		}

		return false;
	}

	public function FindRelation($arelname) 
	{
		foreach ($this->Relations as $rel) {
			if (strtoupper($arelname) == strtoupper($rel->Name))
				return $rel;
		}

		return false;
	}
}