<?php

namespace App\Apps\Generator\Support\Parser\UML;

class Relation 
{
	private $Owner;
	public $Name; // jika tidak ada nama, maka ketika generate DDL lgsg gunakan FK sbg nama.
	public $Linkedid; 
	public $LinkedClass;
	public $isOptional;

	public function __construct($aowner) 
	{
		$this->Owner = $aowner;
	}
	
	public function ResolveLink() 
	{
		$this->LinkedClass = $this->Owner->FindClassByID($this->Linkedid);

		if ($this->Name=='')
			$this->Name = 'L'.$this->LinkedClass->Name;
	}
}