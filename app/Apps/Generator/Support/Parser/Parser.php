<?php

namespace App\Apps\Generator\Support\Parser;

use App\Apps\Generator\Support\Data\ObjectData;
use App\Apps\Generator\Support\Parser\UML\UMLDesign;

abstract class Parser implements ParserContract 
{
	protected $objectData;

    /**
     * Construct UMLParser
     * @param String $file path to uml file
     */
    public function __construct()
    {
        if (method_exists($this, "init"))
            call_user_func_array([$this, "init"], func_get_args());

        $this->createObjectData();
    }

	public function setObjectData(ObjectData $object_data) 
    {
		$this->objectData = $object_data;
	}

	public function getObjectData() 
    {
		return $this->objectData;
	}
}