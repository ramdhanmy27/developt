<?php

namespace App\Apps\Generator\Support\Traits;

trait ObjectClass 
{
	/**
	 * Property Getter
	 * @param String $prop
	 * @param array $args
	 */
	public function __get($prop) 
	{
		$method_name = "get".ucfirst($prop);

		if (method_exists($this, $method_name))
			return call_user_func([$this, $method_name]);
		else
			return $this->{$prop};
	}

	/**
	 * Property Setter
	 * @param String $prop
	 * @param array $args
	 */
	public function __set($prop, $args) 
	{
		$method_name = "set".ucfirst($prop);

		if (method_exists($this, $method_name))
			call_user_func_array([$this, $method_name], $args);
		else
			$this->{$prop} = current($args);
	}

	/**
	 * Fill property by key-value array
	 * will use $default if exists
	 * @param  array  $attr
	 * @param  array[mixed|Closure]  $default
	 */
	public function fillByArray(array $attr, array $default = []) 
	{
		foreach ($this->fillable as $value) {
			if (!isset($attr[$value])) {
				// set default value
				if (isset($default[$value])) {
					if (is_callable($default[$value])) {
						$this->{$value} = $default[$value]($attr);
					} 
					else {
						$this->{$value} = $default[$value];
					}
				}

				continue;
			}

			$this->{$value} = $attr[$value];
		}
	}
}