<?php

namespace App\Apps\Generator\Support\Generator;

use App\Apps\Generator\Repositories\Template;
use App\Apps\Generator\Support\Data\ObjectData;
use App\Apps\Generator\Support\Traits\GetterSetter;
use Exception;

abstract class Generator 
{
	use GetterSetter;

	/**
	 * Parser's Object data
	 * @var ObjectData
	 */
	protected $objectData;

	/**
	 * filesystem instance (root = base_path)
	 * @var FileSystem
	 */
	protected $fs;

	/**
	 * Construct generator
	 * @param ObjectData $object_data Parser's Object data
	 * @param string     $template    generator template location
	 */
	public function __construct(ObjectData $object_data, $template = null) 
	{
		$this->objectData = $object_data;
		$this->fs = app("filesystem")->createLocalDriver(["root" => base_path()]);

		// add generator template to view finder
        // \View::addLocation(base_path("app/Apps/Generator/template"));
        \View::addLocation(Template::instance()->root);

        // set generator template
        $this->template = $template;

        // custom initialization
        if (method_exists($this, "init"))
        	$this->init($object_data);
	}

	/**
	 * write to file
	 * @param  string $template generator template location
	 * @param  string $dest     source destination
	 * @param  array  $attr     template variables
	 * @throws Exception
	 */
	public function make($template, $dest, $attr = null)
	{
		$path = dirname($dest);
        make_dir($path);

		if (!file_exists($path))
			throw new Exception("Failed to write '$dest'.");

		file_put_contents($dest, $this->filter(
			view($template, $attr==null ? $this->get() : $attr)->render()
		));

		// re-format code
		$this->beautify($dest);
	}

	/**
	 * Filter template
	 * @param  string $content template
	 * @return string
	 */
	protected function filter($content)
	{
		return str_replace(
			["<\?php", "##", "{@!!", "{@{"], 
			["<?php", "@", "{!!", "{{"], 
			$content
		);
	}

	/**
	 * Beautify source code
	 * @param  string $type file type
	 * @param  string $path target path
	 */
	public function beautify($path = "./", $type = null)
	{
		switch ($type) 
		{
			case "c":
			case "c++":
				// array multiline indentasinya kejauhan
				// AStyle -UHnxMYxgxM -xT4m0t4 -xC100 --style=allman Bank.php
				break;

			case "java":
				// selalu ada spasi di setiap special char terutama: \ <? php
				// uncrustify -c -xT4m0t4 -xC100 --style=allman Bank.php
				break;
			
			// npm install js-beautify
			case "blade":
			case "php":
			case "html":
			case "css":
			case "js":
			default:
				exec("node \"".base_path("beautify.js")."\" $path");
				
				// PHP CS Fixer
				// array multiline masih belum bener indentasinya
				// tapi bakal ada update untuk fitur ini nantinya (https://github.com/FriendsOfPHP/PHP-CS-Fixer/issues/1438)
				// exec("php ".tmp_path("php-cs-fixer.phar")." fix $path");
				break;
		}
	}

    public function getTemplateAttribute()
    {
    	// dd(static::BASE.".".static::SECTION.".".$this->attributes["template"]);
        return static::BASE.".".static::SECTION.".".$this->attributes["template"];
    }
}