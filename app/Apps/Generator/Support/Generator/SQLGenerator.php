<?php

namespace App\Apps\Generator\Support\Generator;

use App\Apps\Generator\Support\Data\Database\Table;
use Exception;
use ZipArchive;

/**
 * Generate DDL SQL
 * @property  DatabaseObjectData $objectData 
 */
class SQLGenerator extends Generator 
{
	/**
	 * Convert Table instance into SQL Query
	 * @param  Table  $table
	 * @return String
	 */
	protected function generateTable(Table $table) 
	{
		$fields = [];

		foreach ($table->fields as $field) {
			if ($field->isIncrementing)
				$field->type = "serial";

			$column = "$field->name $field->type";

			if (!$field->isIncrementing && $field->length > 0)
				$column .= "($field->length)";

			if ($field->isPrimary)
				$column .= " primary key";

			if (!$field->isNull)
				$column .= " not null";

			$fields[] = $column;
		}

		return "CREATE TABLE $table->name (".implode(", ", $fields).")";
	}

	/**
	 * Generate SQL file
	 * @param  String $path destination path
	 * @return String
	 */
	public function generate($path = ".", $name = null) 
	{
		if ($name == null)
			$name = "export-".date("Y-m-d-his").".sql";

		$path .= "/".$name;

		// write into file
		if (!($open = fopen($path, "w")))
			throw new Exception("Failed to write file.");

		foreach ($this->objectData->getTables() as $table)
			fputs($open, $this->generateTable($table)."\r\n");

		fclose($open);

		return $path;
	}
}