<?php

namespace App\Apps\Generator\Support\Generator\Laravel;

use App\Apps\Generator\Support\Data\Database\Table;
use App\Apps\Generator\Support\Generator\Generator;

class MigrationGenerator extends Generator 
{
    const BASE = "laravel";
    const SECTION = "migration";

	public function generate($path)
	{
        foreach ($this->objectData->getTables() as $table) {
            $this->makeMigration($table, $path);
        }

        return $path;
	}

    public function makeMigration(Table $table, $path)
    {
        $this->set("table", $table->name);
        $this->set("fields", $table->fields);

        $this->make($this->template.".migration", $path."/".date("Y_m_d_his_")."create_{$table->name}.php");
    }

    /** Getter - Setter */

    public function setTableAttribute($name)
    {
        $this->attributes["table"] = $name;
        $this->attributes["class"] = "Create".studly_case($name);
    }
}