<?php

namespace App\Apps\Generator\Support\Generator\Laravel;

use App\Apps\Generator\Support\Data\Database\Table;
use App\Apps\Generator\Support\Generator\Generator;

class CrudGenerator extends Generator 
{
    const BASE = "laravel";
    const SECTION = "crud";

    protected $fillable = [
        "template",         // string : generator template
        "table",            // string : table name
        "fields",           // array[Field] : table fields
        "pk_name",          // string : default: id. inherit: $table
        "url",              // string : inherit: $table
        "name",             // string : Page title. inherit: $table
        "class",            // string : Base Class name. inherit: $table
        "class_id",         // string : Base Class ID. inherit: $table
        "namespace",        // string : [Optional] namespace class. default: app\Http\Controllers
        "model_namespace",  // string : [Optional] Moodels namespace class. default: app\Models
        "base_url",         // string : [Optional] controller's base url
    ];

    public function generate($path)
    {
        $model = new ModelGenerator($this->objectData, $this->attributes["template"]);

        foreach ($this->objectData->getTables() as $table) {
            $this->table = $table;
            $this->fields = $table->fields;
            
            // generate controller & view files
            $this->makeCrud($table, $path);

            // generate model file
            if (isset($this->model_namespace))
                $model->namespace = $this->model_namespace;
            
            $model->makeModel($table, $path."/Models");
        }
        
        return $path;
    }

    /**
     * Generate controller & view files
     * @param  Table  $table
     * @param  string $path  destination path
     */
    public function makeCrud(Table $table, $path)
    {
        // controller id
        $class_id = str_replace("_", "-", $table->name);
        
        // controller class
        $class = studly_case($table->name);

        /** Generate Controller */
        $this->set([
            "name" => id2title($table->name, "_"),
            "class" => $class,
            "url" => (empty($this->base_url) ? "" : $this->base_url."/").$class_id,
            "pk_name" => isset($table->primaryKey) ? $table->primaryKey->name : "id",
        ]);

        $this->make($this->template.".controller", "$path/Controllers/${class}Controller.php");

        /** Generate Views */
        $this->set([
            "name" => id2title($table->name, "_"),
            "class_id" => $class_id,
        ]);

        foreach (["index", "add", "edit", "form", "view"] as $view) {
            $this->make($this->template.".view.$view", "$path/views/$class_id/$view.blade.php");
        }
    }
}