<?php

namespace App\Apps\Generator\Support\Generator\Laravel;

use App\Apps\Generator\Support\Data\DatabaseObjectData;
use App\Apps\Generator\Support\Data\Database\Table;
use App\Apps\Generator\Support\Generator\Generator;

class ModelGenerator extends Generator 
{
    const BASE = "laravel";
    const SECTION = "model";

    protected $fillable = [
        // Generate Model Files
        "template",      // string : generator template
        "table",         // string : table name
        "fields",        // array[Field] : table fields
        "class",         // string : class name. inherit: $table
        "primaryKey",    // Field : [Optional]
        "namespace",     // string : [Optional] namespace class. inherit: $path, default: app\Models.
        "path",          // string : [Optional] -dev- *seems like it's not necessary anymore

        // Eloquent Properties
        "incrementing",  // string : [Optional] inherit: $primaryKey
        "timestamps",    // string : [Optional] 
        "rules",         // string : [Optional] inherit: $fields
        "defaultVal",    // string : [Optional] inherit: $fields
    ];

    public function init(DatabaseObjectData $object_data)
    {
        // $this->set("path", "app/Models");
    }

    public function generate($path)
    {
        // generate model files
        foreach ($this->objectData->getTables() as $table) {
            $this->makeModel($table, $path);
        }

        return $path;
    }

    public function makeModel(Table $table, $path)
    {
        $this->table = $table->name;
        $this->fields = $table->fields;

        // Set Primary key
        if (isset($table->primaryKey))
            $this->primaryKey = $table->primaryKey;

        // Model Fields
        $rules = [];
        $default = [];

        foreach ($table->fields as $field) {
            // skip primary key
            if ($field->isPrimary)
                continue;

            // Default Value
            if (in_array($field->type, ["bool", "boolean"])) {
                // boolean
                $field->defaultValue = true;
            }
            elseif ($field->defaultValue !== null) {
                // any type
                $default[$field->name] = $field->defaultValue;
            }

            // Validation Rules
            $rules[$field->name] = $this->getValidationRules($field);
        }
        
        if (count($rules) > 0)
            $this->rules = array_filter($rules);

        if (count($default) > 0)
            $this->defaultVal = $default;

        $this->make($this->template.".model", $path."/".$this->class.".php");
    }

    private function getValidationRules($field)
    {
        $rules = [];

        if (!$field->isNull)
            $rules[] = "required";

        if (in_array($field->type, ["int", "integer"]))
            $rules[] = "integer";

        if (in_array($field->type, ["numeric", "float", "double", "decimal"]))
            $rules[] = "numeric";

        if ($field->length > 0 && $field->type == "varchar") 
            $rules[] = "max:$field->length";

        return implode("|", $rules);
    }

    /** Getter - Setter */

    public function setPathAttribute($value)
    {
        $this->attributes["path"] = $value;

        $this->attributes["namespace"] = preg_replace_callback(
            "/(^[a-z]|\\\\[a-z])/", 
            function($match) {
                return strtoupper($match[0]);
            }, 
            str_replace("/", "\\", $value)
        );
    }

    public function setTableAttribute($name)
    {
        $this->attributes["table"] = $name;
        $this->attributes["class"] = studly_case($name);
    }
}