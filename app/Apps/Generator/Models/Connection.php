<?php

namespace App\Apps\Generator\Models;

use App\Models\Model;
use Illuminate\Support\Facades\DB;

class Connection extends Model
{
    protected $table = "connection";

    public $timestamps = false;

    protected $fillable = [
        "driver", "name", "username", "password", "prefix", "config",
    ];

    protected $driverInstance;

    /**
     * Column Name
     * @param  string $name
     * @return Connection
     */
    public function attributeLabels()
    {
        return [
            "driver" => "Driver",
            "name" => "Name",
            "username" => "Username",
            "password" => "Password",
            "prefix" => "Prefix",
            // "config" => "Config",
        ];
    }

    public static function driver($driver)
    {
        $conn = config("generator.connection.drivers.$driver");

        if ($conn == null)
            return false;

        return new $conn["class"];
    }

    public function getDriver()
    {
        $driver = self::driver($this->driver);
        $driver->model = $this;

        return $driver;
    }

    /**
     * Make a database connection
     * @param  array|string $database
     * @return Connection
     */
    public function connect($database = null)
    {
        $config = (array) $this->config;
        $config["driver"] = $this->driver;
        $config["username"] = $this->username;
        $config["password"] = $this->password;
        $config["prefix"] = $this->prefix;

        // override database
        if (is_array($database)) {
            if (count($database) == 1) {
                $config["database"] = current($database);
            }
            else if (count($database) > 1) {
                $config["schema"] = current($database);
                $config["database"] = next($database);
            }
        }
        else if ($database != null)
            $config["database"] = $database;

        config(["database.connections.app" => $config]);

        return DB::connection("app");
    }

    /**
     * Check connection availability
     * @return boolean
     */
    public function test()
    {
        try {
            $this->connect()->getPdo();

            return true;
        } 
        catch (Exception $e) {
            return false;
        }
    }

    /** Getter - Setter */

    public function setDriverAttribute($driver)
    {
        $this->attributes["driver"] = $driver;
        $this->config = self::driver($driver)->defaultConfig();
    }

    public function getConfigAttribute()
    {
        return isset($this->attributes["config"]) ? json_decode($this->attributes["config"]) : null;
    }

    public function setConfigAttribute($value)
    {
        $this->attributes["config"] = json_encode($value);
    }

    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (isset($this->config->{$key})) {
            $value = $this->config->{$key};
        }

        return $value;
    }
}