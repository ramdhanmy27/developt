<?php

namespace App\Apps\Generator\Models\Drivers;

interface DriverContract
{
    /**
     * Get database list
     * @return array
     */
    public function databases();

    /**
     * Get table list
     * @param array|string  $database
     * @return array
     */
    public function tables($database);

    /**
     * Default config attributes
     * @return array
     */
    public function defaultConfig();
}