<?php

namespace App\Apps\Generator\Models\Drivers;

use App\Apps\Generator\Models\Connection;
use App\Models\Model;
use DB;

class Sqlite implements DriverContract
{
    const DRIVER = "sqlite";

    public $model;
    
    /**
     * Validation Rules
     * @return array
     */
    public function rules()
    {
        return [
            "driver" => "required",
            "name" => "required|string|max:255",
            "username" => "string|max:255",
            "password" => "string|max:255",
            "prefix" => "string|max:10",
        ];
    }

    /**
     * Get database list
     * @return array
     */
    public function databases() {}

    /**
     * Default config attributes
     * @return array
     */
    public function defaultConfig() {
        return [];
    }
}