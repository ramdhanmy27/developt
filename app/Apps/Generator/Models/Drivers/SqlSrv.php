<?php

namespace App\Apps\Generator\Models\Drivers;

use App\Apps\Generator\Models\Connection;
use App\Models\Model;
use DB;

class SqlSrv implements DriverContract
{
    const DRIVER = "sqlsrv";

    public $model;

    /**
     * Get database list
     * @return array
     */
    public function databases()
    {
        return $this->model->connect()->select(
            "SELECT name FROM master.dbo.sysdatabases"
        );
    }

    /**
     * Validation Rules
     * @return array
     */
    public function rules()
    {
        return [
            "driver" => "required",
            "name" => "required|string|max:255",
            "username" => "string|max:255",
            "password" => "string|max:255",
            "prefix" => "string|max:10",
        ];
    }

    /**
     * Default config attributes
     * @return array
     */
    public function defaultConfig()
    {
        return [
            'host' => "localhost",
            'database' => "",
            'charset' => 'utf8',
        ];
    }
}