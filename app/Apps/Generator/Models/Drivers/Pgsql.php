<?php

namespace App\Apps\Generator\Models\Drivers;

use App\Apps\Generator\Models\Connection;
use App\Models\Model;
use DB;

class Pgsql implements DriverContract
{
    const DRIVER = "pgsql";

    public $model;

    /**
     * Validation Rules
     * @return array
     */
    public function rules()
    {
        return [
            "driver" => "required",
            "name" => "required|string|max:255",
            "username" => "string|max:255",
            "password" => "string|max:255",
            "prefix" => "string|max:10",
        ];
    }

    /**
     * Get database list
     * @return array
     */
    public function databases()
    {
        if (isset($this->model))
            $this->model->connect();

        return DB::connection("app")->select(
            "SELECT datname as name FROM pg_database WHERE datistemplate=false"
        );
    }

    /**
     * Get database list
     * @return array
     */
    public function schemas($database)
    {
        if (isset($this->model))
            $this->model->connect($database);
        
        return DB::connection("app")->select(
            "SELECT nspname as name FROM pg_catalog.pg_namespace"
        );
    }

    /**
     * Get database list
     * @param  array|string $database
     *         e.g: "dbname", ["dbname"], ["schema", "dbname"]
     * @return array
     */
    public function tables($database)
    {
        if (isset($this->model)) {
            $default_schema = "public";

            if (is_string($database))
                $database = [$database, $default_schema];
            else if (count($database) == 1)
                $database[] = $default_schema;
            
            $this->model->connect($database);
        }
        
        return DB::connection("app")->select(
            "SELECT tablename as name FROM pg_catalog.pg_tables 
            where schemaname = ?", 
            [count($database) > 1 ? current($database) : "public"]
        );
    }

    /**
     * Get database list
     * @param  array|string $database
     * @param  array|string $table
     * @return array
     */
    public function fields($database, $table = [])
    {
        if (isset($this->model))
            $this->model->connect($database);

        if (is_string($table))
            $table = [$table];
        
        return DB::connection("app")->select(
            "SELECT 
                c.table_name as table,
                c.column_name as name, 
                column_default as default,
                is_nullable='YES' as is_null,
                udt_name as type,
                character_maximum_length as length,
                p is not null as is_unique,
                p is not null and p.is_primary as is_primary
            FROM information_schema.columns c
            left join (
                SELECT a.attname as name, t.relname as table, i.indisprimary as is_primary
                FROM pg_attribute a
                JOIN pg_index i ON a.attrelid = i.indrelid 
                    AND a.attnum = ANY(i.indkey)
                join pg_class t on t.oid=i.indrelid
            ) p on p.name=c.column_name and p.table=c.table_name
            WHERE table_schema = ? "
                .(count($table) > 0 ? "and c.table_name in ('".implode("','", $table)."') " : "")
            ."order by c.table_name, c.column_name",
            [count($database) > 1 ? current($database) : "public"]
        );
    }

    /**
     * Default config attributes
     * @return array
     */
    public function defaultConfig()
    {
        return [
            'host' => "localhost",
            'port' => 5432,
            'database' => "postgres",
            'charset' => 'utf8',
            'schema' => 'public',
        ];
    }
}