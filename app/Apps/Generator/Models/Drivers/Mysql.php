<?php

namespace App\Apps\Generator\Models\Drivers;

use App\Apps\Generator\Models\Connection;
use App\Models\Model;
use DB;

class Mysql implements DriverContract
{
    const DRIVER = "mysql";

    public $model;
    
    /**
     * Validation Rules
     * @return array
     */
    public function rules()
    {
        return [
            "driver" => "required",
            "name" => "required|string|max:255",
            "username" => "string|max:255",
            "password" => "string|max:255",
            "prefix" => "string|max:10",
        ];
    }

    /**
     * Get database list
     * @return array
     */
    public function databases()
    {
        return $this->model->connect()->select("SHOW DATABASES");
    }

    /**
     * Default config attributes
     * @return array
     */
    public function defaultConfig()
    {
        return [
            'host' => "localhost",
            'port' => 3306,
            'database' => "information_schema",
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'strict' => false,
            'engine' => null,
        ];
    }
}