<?php

namespace App\Apps\Generator\Models;

use App\Models\Model;
use Illuminate\Support\Facades\DB;

class ParserTemplate extends Model
{
    protected $table = "parser_template";

    protected $fillable = ["name", "parser", "value"];

    public $timestamps = false;

    /** Getter - Setter */

    public function getValueAttribute()
    {
        return isset($this->attributes["value"]) ? json_decode($this->attributes["value"]) : null;
    }

    public function setValueAttribute($value)
    {
        $this->attributes["value"] = json_encode($value);
    }
}