var path = require("path");
var elixir = require("laravel-elixir");

var mlixir = function(dir, callback) {
	// initialize asset path
	elixir.config.assetsPath = path.join(dir, "assets");
	elixir.config.publicPath = path.join("public/app", mlixir.camel2id(path.basename(dir)));

	// module path
	mlixir.path = {
		css: elixir.config.publicPath +"/css/",
		js: elixir.config.publicPath +"/js/",
	}

	// execute elixir callback
	elixir(callback);
};

mlixir.camel2id = function(str) {
	return str.replace(/([A-Z])/g, "-$1").replace(/^\-/, "").toLowerCase();
};

module.exports = mlixir;