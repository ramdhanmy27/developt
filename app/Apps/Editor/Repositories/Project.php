<?php

namespace App\Apps\Editor\Repositories;

use App\Services\Traits\Singleton;
use Illuminate\Support\Facades\Storage;

class Project
{
	private $disk;

	public function __construct($root, $visibility = "public")
	{
		$this->disk = Storage::createLocalDriver(compact("root", "visibility"));
	}

	public function __call($method, $args)
	{
		return call_user_func_array([$this->disk, $method], $args);
	}

	public function all()
	{
		return collect($this->disk->directories("/"));
	}
}