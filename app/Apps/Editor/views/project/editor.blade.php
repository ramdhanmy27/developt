@extends("app")

@section("title", $project_name)

@section("content")
	<div id="editor" class="row">
		<div class="col-md-3">
			<div id="tree-folder" class="row">
				
			</div>
		</div>
		<div class="col-md-9">
			<div id="code-editor" class="row">

			</div>
		</div>
	</div>
@endsection

@push("style")
	<style type="text/css">
		html, body, .content-body, .full-height, #editor {
			height: 100%;
		}

		#editor {
			background: #09f;
		}
	</style>
@endpush

@push("script")
	<script type="text/javascript" src="{{ asset("js/lib/vue.min.js") }}"> </script>
	<script type="text/javascript" src="{{ asset("js/lib/ace/ace.js") }}"> </script>
	<script type="text/javascript" src="{{ asset("js/app/editor/editor.js") }}"> </script>
@endpush