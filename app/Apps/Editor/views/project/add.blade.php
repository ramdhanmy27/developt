@extends("app")

@section("content")
	<form method="post" 
		@if (isset($url)) action="{{ url($url) }}" @endif 
		class="form-horizontal">
		{{ csrf_field() }}

		<div class="form-group">
			<label class="control-label col-md-3">Name</label>

			<div class="col-md-6">
				<input type="text" name="name" class="form-control" />
			</div>
			
			<div class="col-md-3">
				<button type="submit" class="btn btn-primary"> Simpan </button>
			</div>
		</div>
	</form>
@endsection