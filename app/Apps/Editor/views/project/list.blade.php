@extends("app")

@section("content")
	<div class="row">
		<a href="{{ url("editor/$base/$section/add") }}" class="btn btn-primary" modal>
			<i class="fa fa-plus"></i> New Project
		</a>
	</div><br>

	<div class="table-responsive row">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Project Name</th>
					<th>Last Modified</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($projects as $name)
					<tr>
						<td>{{ $name }}</td>
						<td>{{ date("d/m/y h:i", $repo->lastModified($name)) }}</td>
						<td>
							<a href="{{ url("editor/$base/$section/project/$name") }}" 
								class="btn btn-sm btn-warning"
								target="_blank"
								tooltip="Open Project">
								<i class="fa fa-edit"></i>
							</a>

							<a modal
								href="{{ url("editor/$base/$section/duplicate/$name") }}" 
								class="btn btn-sm btn-default" 
								tooltip="Duplicate">
								<i class="fa fa-copy"></i>
							</a>

							<a href="{{ url("editor/$base/$section/delete/$name") }}" 
								class="btn btn-sm btn-danger" 
								method="post"
								confirm="@lang("confirm.delete")"
								tooltip="Remove">
								<i class="fa fa-remove"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection

@push("style")
	<style type="text/css">
		table tr td {
			vertical-align: middle !important;
		}
	</style>
@endpush