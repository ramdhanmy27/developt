<?php

namespace App\Apps\Editor\Controllers;

use App\Apps\Editor\Repositories\Project;
use App\Facades\Flash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
	private $project;

	public function callAction($method, $param)
	{
		$this->project = new Project(config("editor.path")."/$param[base]/$param[section]");

		view()->share("base", $param["base"]);
		view()->share("section", $param["section"]);

		return parent::callAction($method, $param);
	}

	public function index()
	{
		return view("project.list", [
			"projects" => $this->project->all(),
			"repo" => $this->project,
		]);
	}

	public function add(Request $req, $base, $section)
	{
		if ($req->isMethod("post")) {
			if ($this->project->exists($req->name)) {
				Flash::danger("Project '$req->name' already exists.");
				return back();
			}
			else if (preg_match("/[^\w\s\.\(\)\-\+]/", $req->name)) {
				Flash::danger("Project name was invalid.");
				return back();
			}

			$this->project->makeDirectory($req->name);
			return redirect("editor/open/$req->name");
		}

		return view("project.add", ["url" => "editor/$base/$section/add"]);
	}

	public function duplicate(Request $req, $base, $section, $project_name)
	{
		if ($req->isMethod("post")) {
			$this->add($req);

			foreach ($this->project->allFiles($project_name) as $file) {
				$this->project->copy($file, $req->name.substr($file, strpos($file, "/")));
			}

			return redirect("editor/open/$req->name");
		}

		return view("project.add", ["url" => "editor/$base/$section/duplicate/$project_name"]);
	}

	public function delete($project_name)
	{
		if ($this->project->exists($project_name)) {
			Flash::danger("Project '$project_name' already exists.");
			return back();
		}
		
		$this->project->deleteDirectory($project_name);

		return back();
	}

	public function openProject($project_name)
	{
		return view("project.editor", compact("project_name"));
	}
}