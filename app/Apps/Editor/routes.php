<?php

Route::group(["prefix" => "{base}/{section}"], function() {
	Route::get("/", "ProjectController@index");
	Route::match(["get", "post"], "add", "ProjectController@add");

	Route::match(["get", "post"], "duplicate/{project}", "ProjectController@duplicate");
	Route::post("delete/{project}", "ProjectController@delete");

	Route::group(["prefix" => "project/{project}"], function() {
		Route::get("/", "ProjectController@openProject");
	});
});