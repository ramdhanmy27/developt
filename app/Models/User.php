<?php 

namespace App\Models;

use App\Models\Penyedia;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait;

    protected $fillable = ["email", "password", "kode_penyedia"];

    protected $hidden = ["password", "remember_token"];

    public static $rules = [
        "email" => "required|email|max:255",
    	"kode_penyedia" => "required|max:10",
    ];

    public function registerRoles(array $roles) 
    {
        $data = [];
        $id = $this->getKey();

        foreach ($roles as $role_id)
            $data[] = ["role_id" => $role_id, "user_id" => $id];

        DB::table("role_user")->where("user_id", $id)->delete();
        DB::table("role_user")->insert($data);
    }

    public static function getRandomPass()
    {
        return str_random(rand(5, 7));
    }

    public function vendor()
    {
        return $this->belongsTo(Penyedia::class, "kode_penyedia", "kode");
    }

    public function typeof()
    {
        return $this->vendor->kode == $type;
    }
}