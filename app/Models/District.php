<?php

namespace App\Models;


class District extends Model
{
	protected $table = "district";
	
	protected $primaryKey = "code";

	public $timestamps = false;
}
