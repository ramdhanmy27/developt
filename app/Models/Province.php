<?php

namespace App\Models;


class Province extends Model
{
	protected $table = "province";
	
	protected $primaryKey = "code";

	public $timestamps = false;
}
