<?php

namespace App\Models;

use App\Exceptions\ValidatorException;
use LaravelArdent\Ardent\Ardent;
use Validator;
use Closure;

abstract class Model extends Ardent 
{
    public function label($field) 
    {
        if (method_exists($this, "attributeLabels"))
            $label = $this->attributeLabels();
        
        return isset($label[$field]) ? $label[$field] : id2title($field, "[-_]");
    }

    /**
     * @inheritDoc
     */
    public static function createOrFail(array $data = []) 
    {
        $model = new static($data);

        $args = func_get_args();
        array_shift($args);
        call_user_func_array([$model, "saveOrFail"], $args);

        return $model;
    }

    /**
     * @inheritDoc
     */
    public function updateOrFail(array $data = [], array $opt = []) 
    {
        if (!$this->exists)
            return false;

        return $this->fill($data)->saveOrFail([], [], $opt);
    }

    /**
     * Save the model to the database using transaction.
     *
     * @param  array  $options
     * @return bool
     *
     * @throws \Throwable
     */
    public function saveOrFail(
        array $rules = [], 
        array $msg = [], 
        array $opt = [], 
        Closure $before = null, 
        Closure $after = null) 
    {
        $success = $this->save($rules, $msg, $opt, $before, $after);

        if (!$success)
            throw new ValidatorException($this->validator);

        return $success;
    }

    /**
     * Validate data input
     * 
     * @param  array $data
     * @return boolean
     */
    public static function validateData(
        array $data, 
        array $rules = [], 
        array $customMessages = [], 
        array $customAttributes = []) 
    {
        $validator = static::makeValidator($data, 
            empty($rules) ? static::$rules : $rules, 
            empty($customMessages) ? static::$customMessages : $customMessages, 
            empty($customAttributes) ? static::$customAttributes : $customAttributes
        );

        return $validator->passes();
    }

    /**
     * Validate data input
     * 
     * @param  array $data
     * @return boolean
     */
    public static function validateDataOrFail(
        array $data, 
        array $rules = [], 
        array $customMessages = [], 
        array $customAttributes = []) 
    {
        $validator = static::makeValidator($data, 
            empty($rules) ? static::$rules : $rules, 
            empty($customMessages) ? static::$customMessages : $customMessages, 
            empty($customAttributes) ? static::$customAttributes : $customAttributes
        );

        if ($validator->fails())
            throw new ValidatorException($validator);
    }

    public function getRules() 
    {
        return isset(static::$rules) ? static::$rules : null;
    }
}
