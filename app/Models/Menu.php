<?php

namespace App\Models;

class Menu extends Model 
{
	protected $table = "menu";
	
	protected $fillable = ["title", "url", "parent", "enable", "order"];

	public $timestamps = false;
}
