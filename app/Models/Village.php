<?php

namespace App\Models;


class Village extends Model
{
	protected $table = "village";
	
	protected $primaryKey = "code";

	public $timestamps = false;
}
