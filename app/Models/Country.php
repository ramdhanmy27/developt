<?php

namespace App\Models;


class Country extends Model
{
	protected $table = "country";
	
	protected $primaryKey = "iso_code";

	public $timestamps = false;
}
