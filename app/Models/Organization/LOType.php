<?php

namespace App\Models\Organization;

use App\Models\Model;

class LOType extends Model
{
    protected $table = "lotype";

    protected $fillable = [
        "code",
        "name",
        "description",
        "is_active",
    ];

    public static $rules = [
        "code" => "required|max:10",
        "name" => "required|max:40",
        "description" => "max:255",
    ];

    protected $attributes = [
        "is_active" => true,
    ];

    public $timestamps = false;
}
