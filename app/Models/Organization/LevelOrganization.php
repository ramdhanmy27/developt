<?php

namespace App\Models\Organization;

use App\Models\Model;

class LevelOrganization extends Model
{
    protected $table = "level_organization";
    
    protected $fillable = [
    	"code",
		"name",
		"description",
		"location_id",
		"lotype_id",
		"cost_center_group_id",
	];

	public static $rules = [
		"code" => "max:10",
		"name" => "max:40",
		"description" => "max:255",
		"location_id" => "exists:location,location_id",
		"lotype_id" => "exists:lotype,lotype_id",
		"cost_center_group_id" => "exists:cost_center_group,cost_center_group_id",
	];

    public $timestamps = false;
}
