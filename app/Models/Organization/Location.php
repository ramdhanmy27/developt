<?php

namespace App\Models\Organization;

use App\Models\Model;

class Location extends Model
{
    protected $table = "location";
    
    protected $fillable = [
		"location_id",
    	"code",
		"locname",
		"locno",
		"address",
		"zipcode",
		"isactive",
		"company_information_id",
		"company_level_id",
		"company_sector_id",
		"city_id",
		"country_id",
		"province_id",
		"line_of_business_id",
	];

	public static $rules = [
		"location_id" => "required|max:32",
    	"code" => "max:10",
		"locname" => "max:40",
		"locno" => "max:10",
		"address" => "max:255",
		"zipcode" => "max:10",
		"isactive" => "boolean",
		"company_information_id" => "exists:company_information,company_information_id",
		"company_level_id" =>  "exists:company_level,company_level_id",
		// "company_sector_id" => "exists:company_sector,company_sector_id",
		"city_id" => "exists:city,city_id",
		"country_id" => "exists:country,country_id",
		"province_id" => "exists:province,province_id",
		"line_of_business_id" => "exists:line_of_business,line_of_business_id",
	];

    protected $primaryKey = "location_id";

    public $incrementing = false;

    // public $timestamps = false;
}
