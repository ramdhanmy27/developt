<?php

namespace App\Models\Organization;

use App\Models\Model;

class LineOfBusiness extends Model
{
    protected $table = "line_of_business";

    protected $fillable = [
        "code",
        "name",
        "description",
        "is_active",
    ];

    public static $rules = [
        "code" => "required|max:10",
        "name" => "required|max:40",
        "description" => "max:255",
    ];

    protected $attributes = [
        "is_active" => true,
    ];

    public $timestamps = false;
    
    public function attributeLabels() 
    {
        return [
            "code" => trans("ref.lob.column.code"),
            "name" => trans("ref.lob.column.name"),
        ];        
    }
}
