<?php

namespace App\Models\Organization;

use App\Models\Model;

class CostCenterGroup extends Model
{
    protected $table = "cost_center_group";

    protected $fillable = [
        "code",
        "name",
        "description",
    ];

    public static $rules = [
        "code" => "required|max:10",
        "name" => "required|max:40",
        "description" => "max:255",
    ];
    
    public $timestamps = false;
}
