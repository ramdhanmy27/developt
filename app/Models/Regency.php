<?php

namespace App\Models;


class Regency extends Model
{
	protected $table = "regency";
	
	protected $primaryKey = "code";

	public $timestamps = false;
}
