<?php

namespace App\Facades;

class Notif extends \Illuminate\Support\Facades\Facade {
	
	protected static function getFacadeAccessor() {
		return "notif";
	}
}