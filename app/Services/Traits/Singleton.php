<?php

namespace App\Services\Traits;

trait Singleton
{
	private static $instance;

	private function __construct() {}

	public static function instance()
	{
		if (static::$instance === null) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	public static function __callStatic($method, $args)
	{
		return call_user_func_array([$this->instance(), $method], $args);
	}
}