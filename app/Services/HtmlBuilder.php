<?php

namespace App\Services;

class HtmlBuilder extends \Collective\Html\HtmlBuilder 
{
    public function icon($class) 
    {
        return "<i class='$class'></i>";
    }

    /**
     * Generate a HTML link.
     *
     * @param string $url
     * @param string $title
     * @param array  $attributes
     * @param bool   $secure
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function link($url, $title = null, $attributes = [], $secure = null, $escape = true)
    {
        $url = $this->url->to($url, [], $secure, $escape);

        if (is_null($title) || $title === false) {
            $title = $url;
        }

        return $this->toHtmlString(
            '<a href="' . $url . '"' . $this->attributes($attributes) . '>' . $title . '</a>'
        );
    }

    public function viewLabel($model, $field)
    {
        return view("ui.view-label", [
            "field" => $model->label($field),
            "value" => $model->{$field},
        ]);
    }
}