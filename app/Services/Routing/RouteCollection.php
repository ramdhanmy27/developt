<?php

namespace App\Services\Routing;

class RouteCollection extends \Illuminate\Routing\RouteCollection {

    public function addItem(Route $route) {
        $this->addToCollections($route);
        $this->addLookups($route);

    	return $route;
    }
}