<?php

namespace App\Services;

use App\Models\Model;

class FormBuilder extends \Collective\Html\FormBuilder 
{
    protected $rules = [];

    protected $labels = [];

    /** Misc */

    public function model($model, array $options = []) 
    {
        if (is_string($model)) {
            if (isset($model::$rules))
                $this->rules = $model::$rules;

            $model = new $model;
        }

        $this->labels = method_exists($model, "attributeLabels") ? $model->attributeLabels() : [];

        if (method_exists($model, "getRules"))
            $this->rules = $model->getRules();

        return parent::model($model, $options);
    }

    public function rules($rules) 
    {
        if (!is_array($rules))
            return;

        $this->rules = array_merge($rules, $this->rules);
    }

    public function open(array $options = []) 
    {
        return parent::open($options + ["class" => "form-horizontal form-bordered"]);
    }

    public function label($name, $value = null, $options = [], $secure = null, $escape = true) 
    {
        return parent::label(
            $name, $value, $options + ["class" => "col-md-3 control-label"], $secure, $escape
        );
    }

    public function value($name, $value = null, $options = []) 
    {
        if ($value===null && isset($this->model) && isset($this->model->{$name}))
            $value = $this->model->{$name};

        return parent::label(
            $name, $value, $options + ["class" => "col-md-3 control-label text-left"]
        );
    }

    public function group($type, $name, $label = null, $value = null, $options = []) 
    {
        $type = str_replace("-", "", $type);

        // handle input type with existing method
        if (method_exists($this, $type)) {
            // reorder arguments
            $param = array_slice(func_get_args(), 3);
            array_unshift($param, $name);

            // param : name, value, options
            $input = call_user_func_array([$this, $type], $param);
        }
        // handle with default input
        else $input = $this->input($type, $name, $value, $options);

        if ($label===null) {
            if (method_exists($this->model, "label"))
                $label = $this->model->label($name);
            else
                $label = id2title($name, "[-_]");
        }

        return "<div class='form-group'>".
                    $this->label($name, $label)
                    ."<div class='col-md-7'>$input</div>"
                ."</div>";
    }

    public function close() 
    {
        $this->rules = null;
        $this->labels = null;

        return parent::close();
    }

    /** Inputs */

    public function input($type, $name, $value = null, $options = []) 
    {
        return parent::input($type, $name, $value, $this->getInputOptions($name, $options));
    }

    public function img($name, $preview = null, $options = [])
    {
        if ($preview === null)
            $preview = $this->getModelValueAttribute($name);

        if (!empty($preview))
            $options["preview"] = url($preview);

        return $this->file($name, $options);
    }

    public function date($name, $value = null, $options = []) 
    {
        $options["datepicker"] = "";
        return $this->text($name, $value, $this->getInputOptions($name, $options + ["datepicker" => ""]));
    }

    public function dateRange($name, $value = null, $options = []) 
    {
        return $this->text($name, 
            is_array($value) ? implode(" - ", $value) : $value, 
            $this->getInputOptions($name, $options + ["datepicker-range" => ""])
        );
    }

    public function datetime($name, $value = null, $options = []) 
    {
        return parent::datetime($name, $value, $this->getInputOptions($name, $options));
    }

    public function time($name, $value = null, $options = []) 
    {
        $options["timepicker"] = "";
        return $this->text($name, $value, $this->getInputOptions($name, $options));
    }

    public function select($name, $list = [], $selected = null, $options = []) 
    {
        if (isset($this->model) && $selected === null)
            $selected = ($this->model->{$name});

        return parent::select($name, $list, $selected, $this->getInputOptions(
            $name, $options + [
                "select2" => "", 
                "placeholder" => "Select..", 
                "value" => is_array($selected) ? implode(",", $selected) : $selected,
            ]
        ));
    }

    public function checkbox($name, $value = 1, $checked = null, $options = []) 
    {
        $id = isset($options["id"]) ? $options["id"] : $name.date("His").rand();

        return "<div class='checkbox-custom ".@$options['class']."'>"
                    .parent::checkbox($name, $value, $checked, $this->getInputOptions($name, $options + ["id" => $id]))
                    ."<label for='$id'>".@$options['label']."</label>
                </div>";
    }

    public function checkboxes($name, $options = [], $checked = []) 
    {
        $html = [];

        foreach ($options as $value => $label)
            $html[] = $this->checkbox(
                $name, $value, 
                count($checked)==0 ? null : in_array($value, $checked), 
                ["label" => $label]
            );

        return implode("", $html);
    }

    public function toggleSwitch($name, $value = 1, $checked = null, $options = []) 
    {
        $options["switch"] = "";

        return "<div class='switch ".@$options["class"]."'>"
                    .parent::checkbox($name, $value, $checked, $options)
                ."</div>";
    }

    public function radio($name, $value = null, $checked = null, $options = []) 
    {
        $id = isset($options["id"]) ? $options["id"] : $name.date("His").rand();

        return "<div class='radio-custom ".@$options['class']."'>"
                    .parent::radio($name, $value, $checked, $this->getInputOptions($name, $options + ["id" => $id]))
                    ."<label for='$id'>".@$options['label']."</label>
                </div>";
    }

    public function radios($name, $options = [], $checked = []) 
    {
        $html = [];
        foreach ($options as $value => $label)
            $html[] = $this->radio($name, $value, in_array($value, $checked), ["label" => $label]);

        return implode("", $html);
    }

    public function textarea($name, $value = null, $options = ["rows" => 5]) 
    {
        return parent::textarea($name, $value, $this->getInputOptions($name, $options));
    }

    /** Buttons */

    public function submit($value = null, $options = []) 
    {
        return parent::submit($value, $options + ["class" => "btn btn-primary"]);
    }

    public function button($value = null, $options = []) 
    {
        return parent::button($value, $options + ["class" => "btn btn-default"]);
    }

    /** Methods */

    public function getFieldRule($name) 
    {
        if ($this->rules == null || !isset($this->rules[$name]))
            return null;

        return $this->rules[$name];
    }

    protected function getInputOptions($name = null, $options = []) 
    {
        $defaultOpt = ["class" => "form-control"];

        if ($rules = $this->getFieldRule($name))
            $defaultOpt["rules"] = $rules;

        if ($name != null)
            $defaultOpt += ["id" => $name];

        return $options + $defaultOpt;
    }

    protected function option($display, $value, $selected)
    {
        return parent::option(trans($display), $value, $selected);
    }
}