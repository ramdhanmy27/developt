<?php

return [
    "path" => [
        "base" => base_path("app/Apps/Generator"),
        "tmp" => base_path("storage/generator"),
        "template" => config("generator.path.tmp")."/template",
    ],

    "connection" => [
        "drivers" => [
            "mysql" => [
                "name" => "MySQL",
                "class" => App\Apps\Generator\Models\Drivers\Pgsql::class,
            ],
            "pgsql" => [
                "name" => "PostgreSQL",
                "class" => App\Apps\Generator\Models\Drivers\Pgsql::class,
            ],
            "sqlite" => [
                "name" => "SQLite",
                "class" => App\Apps\Generator\Models\Drivers\Sqlite::class,
            ],
            "sqlsrv" => [
                "name" => "SQL Server",
                "class" => App\Apps\Generator\Models\Drivers\SqlSrv::class,
            ],
        ],
    ],

    "parser" => [
        "uml" => [
            "title" => "UML",
            "class" => App\Apps\Generator\Support\Parser\UMLParser::class,
        ],
        "database" => [
            "title" => "Database",
            "class" => App\Apps\Generator\Support\Parser\DatabaseParser::class,
        ],
    ]
];