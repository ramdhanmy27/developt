<?php

return [
    "default" => env("MENU_DRIVER", "router"),

	"separate" => false,

	"drivers" => [
		"db" => env("DB_CONNECTION", "pgsql"),
	],
];