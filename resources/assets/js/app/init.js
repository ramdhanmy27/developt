/** JQuery */
$.ajaxSetup({ headers: {'X-CSRF-Token': fn.csrf} });

$(document).ready(function() {
    fn.event.init(document);

    // override functions
    window.alert = fn.alert;
    window.confirm = function(message, callback) {
        fn.confirm({
            title: "Confirmation",
            body: message,
            yes: function() {
                callback(true);
            },
            no: function() {
                callback(false);
            },
        });
    }
});