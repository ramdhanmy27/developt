<?php

return [
    "back" => "Kembali",

    // data list
    "view" => "Lihat",
    "update" => "Ubah",
    "delete" => "Hapus",

    // form
    "create" => "Buat Baru",
    "select" => "Pilih",
    "save" => "Simpan",

    // confirmation
    "yes" => "Ya",
    "no" => "Tidak",

    // account
    "logout" => "Keluar",
];