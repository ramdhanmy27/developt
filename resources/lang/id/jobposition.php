<?php

return [
	'is this the real life' => 'mom\'s spaghetti',

	// Job Grade
	'grade' => [
		'code' => 'Kode',
		'name' => 'Nama',
	],

	// Employee Grade
	'employee_group' => [
		'code' => 'Kode Grup Pegawai',
		'name' => 'Nama Grup Pegawai',
		'description' => 'Deskripsi',
	],

	// Pay Grade
	'pay_grade' => [
		'code' => 'Kode Pay Grade',
		'name' => 'Nama Pay Grade',
		'is_active' => 'Aktif',
		'currency' => 'Mata Uang',
		'starting_salary' => 'Gaji Minimum',
		'most_salary' => 'Gaji Rata-Rata',
		'end_salary' => 'Gaji Maksimum',
	],

	// Position
	'position' => [
		'code' => 'Kode Posisi',
		'name' => 'Nama Posisi',
		'id_parent' => 'Posisi Atas',
		'is_active' => 'Aktif',
		'description' => 'Deskripsi',
		'id_employee_group' => 'Grup Pegawai',
		'id_level_organization' => 'Level Organisasi',
		'id_location' => 'Lokasi',
		'id_factory' => 'Pabrik',
		'grades' => 'Grade',
		'job_status' => 'Status Pekerjaan',
		'type' => 'Tipe Posisi',
	],
];