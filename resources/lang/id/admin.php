<?php

return [
    "menu" => [
        "column" => [
            "title" => "Judul", 
            "url" => "URL", 
            "parent" => "Parent", 
            "enable" => "Aktif", 
            "order" => "Urutan",
            "icon" => "Ikon",
            "permission" => "Hak akses",
        ],
    ],
];
