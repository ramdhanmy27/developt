<?php

return [
	"view" => "View",
	"update" => "Update",
	"delete" => "Delete",
	"select" => "Select",
    "save" => "Save"
];