<?php

return [
	'is this the real life' => 'mom\'s spaghetti',

	// Job Grade
	'grade' => [
		'code' => 'Code',
		'name' => 'Name',
	],

	// Employee Grade
	'employee_group' => [
		'code' => 'Employee Group Code',
		'name' => 'Employee Group Name',
		'description' => 'Description',
	],

	// Pay Grade
	'pay_grade' => [
		'code' => 'Pay Grade Code',
		'name' => 'Pay Grade Name',
		'is_active' => 'Active',
		'currency' => 'Currency',
		'starting_salary' => 'Minimum Pay',
		'most_salary' => 'Middle Pay',
		'end_salary' => 'Maximum Pay',
	],

	// Position
	'position' => [
		'code' => 'Position Code',
		'name' => 'Position Name',
		'id_parent' => 'Parent Position',
		'is_active' => 'Active',
		'description' => 'Description',
		'id_employee_group' => 'Employee Group',
		'id_level_organization' => 'Organization Level',
		'id_location' => 'Location',
		'id_factory' => 'Factory',
		'grades' => 'Grade',
		'job_status' => 'Job Status',
		'type' => 'Position Type',
	],
];