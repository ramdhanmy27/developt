<?php

return [
	// Bank Group
	'bank_group' => [
		'code' => 'Code',
		'name' => 'Group Name',
	],

	// Bank
	'bank' => [
		'branch' => 'Branch',
		'code' => 'Bank Code',
		'name' => 'Bank Name',
		'address' => 'Address',
		'bank_group' => 'Bank Group',
		'zip_code' => 'Postal Code',
		'regency_code' => 'Regency',
		'bank_group_code' => 'Bank Group',
	],
];