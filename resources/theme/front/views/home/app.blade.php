@extends('front::layout.layout')

@push("script")
    <script src="{{ asset("front::js/public/slider.js") }}"></script>
@endpush

@push("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("front::css/public/slider.css") }}" />
@endpush

@section('body')
    {!! app("slider")->render() !!}

    <div class="container">
        <div class="row mt-xlg">
            @yield("content")
        </div>
    </div>
@endsection