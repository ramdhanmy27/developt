<ol class="breadcrumbs">
    <li> <a href="{{ url("/") }}"> <i class="fa fa-home"></i> </a> </li>

    @foreach ($path as $menu)
	    <li> 
	    	{{-- Active Breadcrumb--}}
	    	@if ($count === $i+1)
	    		{{ $menu["title"] }}
	    	@else
		    	<a href="{{ $menu["url"]!==null ? url($menu["url"]) : "#" }}"> 
		    		<span>{{ $menu["title"] }}</span>
		    	</a>
	    	@endif
	    </li>
    @endforeach
</ol>
