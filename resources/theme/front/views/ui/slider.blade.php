<?php

use App\Services\Widget\Slider;

?>

<div class="slider-container rev_slider_wrapper">
    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider 
        data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 500}'>
        <ul>
            @foreach ($list as $item)
                <li data-transition="fade">
                    <img src="{{ asset("front::".Slider::PATH."/$item") }}"
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat" 
                        class="rev-slidebg">
                </li>
            @endforeach
        </ul>
    </div>
</div>