var t = require('../themelixir');

module.exports = function() {
    t.mix("backend", function(mix) {
        mix

        //////////
        // LESS //
        //////////
        
        .less('all.less', t.tmp.css + "all.css")
        
        /////////
        // CSS //
        /////////

        // application theme
        .styles([
            "theme/theme.css",
            "theme/skin.css",

            // all less
            t.tmp.css + "all.css",
        ], t.pub.css + "app.css")
        
        // vendor
        .styles([
            t.shared.css + "bootstrap.min.css",
            t.shared.css + "font-awesome.min.css",
            "ui/jquery.dataTables.min.css",
            "ui/datatables.css",
            "ui/pnotify.custom.css",
        ], t.pub.css + "vendor.css")

        // form input
        .styles([
            t.shared.css + "input/datepicker3.css",
            t.shared.css + "input/bootstrap-timepicker.min.css",
            t.shared.css + "input/dropzone/dropzone.css",
            t.shared.css + "input/bootstrap-fileupload.min.css",
            t.shared.css + "input/select2.css",
        ], t.pub.css + "input.css")

        ////////////////
        // Javascript //
        ////////////////

        // application theme
        .scripts([
            "ui/jquery.dataTables.min.js",
            "theme/theme.js",

            // App JS
            t.shared.js + "fn.js",
            t.shared.js + "angular.init.js",
            "app/event.js",
            "app/init.js",
        ], t.pub.js + "app.js")
        
        // vendor
        .scripts([
            t.shared.js + "jquery.min.js",
            t.shared.js + "bootstrap.min.js",
            // t.shared.js + "angular.min.js",
            "theme/nanoscroller.js",
            "theme/modernizr.js",
            "ui/pnotify.custom.js",
        ], t.pub.js + "vendor.js")

        // form input
        .scripts([
            t.shared.js + "input/validator.min.js",
            t.shared.js + "input/bootstrap-maxlength.js",
            t.shared.js + "input/bootstrap-datepicker.js",
            t.shared.js + "input/bootstrap-timepicker.js",
            t.shared.js + "input/bootstrap-fileupload.min.js",
            t.shared.js + "input/dropzone.min.js",
            t.shared.js + "input/select2.min.js",
            t.shared.js + "input/ios7-switch.js",
        ], t.pub.js + "input.js")
    });
}