<div class="col-md-6">
    <div class='row'>
        <div class='col-md-3'> <b>{{ $field }}</b> </div>
        <div class="col-md-9">: {!! $value or "<i class='text-muted'>(null)</i>" !!}</div>
    </div>
</div>