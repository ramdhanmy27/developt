var beautify = require("js-beautify");
var fs = require("fs");
var glob = require("glob");

var fixer = {
    php: function(data) {
        return beautify.js_beautify(data, {
            brace_style: "expand",
            // brace_style: "collapse",
            end_with_newline: true,
            indent_with_tabs: false,
            // break_chained_methods: false,
            // operator_position: "before-newline",
            // operator_position: "after-newline",
            // operator_position: "preserve-newline",
        })
            // fix beautify miss
            .replace(/<< \? php/g, "<?php")
            .replace(/\s+-\s+>\s+/g, "->")
            .replace(/ (of) /g, "$1")
            .replace(/(\w)\\\s+/g, "$1\\")
            .replace(/(public|private|protected)\s+function/g, "$1 function")
    },
    blade: function(code) {
        return beautify.html(code, {
            end_with_newline: true, //            End output with newline
            preserve_newlines: true, //           Preserve existing line_breaks (__no_preserve_newlines disables)
            brace_style: "collapse", //                 [collapse_preserve_inline|collapse|expand|end_expand|none] ["collapse"]
            // wrap_attributes: "auto",
            // content_unformatted: ["a", "pre"], 
        })
            // .replace(/(.)(@\w+)/g, "$1\n$2")
            // .replace(/(\{\!\!|\{\{)(.+?)(\!\!\}|\}\})/g, "\n$1$2$3")
    },
    form_blade: function(code) {
        return code
            .replace(/\n\s*?\{\!\!\s*Form::group/gi, "\n\t{!! Form::group");
    },
    fix: function(file) {
        // menentukan type file
        if (file.indexOf("form.blade.php") != -1) {
            var type = "form_blade";
        }
        else if (file.indexOf(".blade.php") != -1) {
            // karena belum ada beautifier yang bagus untuk laravel blade, sementara diabaikan
            return;
            var type = "blade";
        }
        else {
            // ambil ekstensi file sebagai type
            var type = file.substr(file.lastIndexOf(".") + 1);
        }

        // hentikan proses karena type tidak dikenal
        if (!fixer.hasOwnProperty(type)) {
            console.log("Failed: " + type + " : " + file);
            return;
        }

        // show formatted files
        // console.log(type + " : " + file);

        // beautify code & rewrite
        var code = fs.readFileSync(file).toString();

        fs.writeFileSync(file, fixer[type](code)); 
    },
}

var filepath = process.argv[2];

// fix formatting inside directories
if (fs.lstatSync(filepath).isDirectory()) {
    glob(filepath + "/**/*.*", function(err, files) {
        if (err) throw err;

        for (var i in files) {
            fixer.fix(files[i]);
        }
    });
}
// fix a single file
else fixer.fix(filepath);
