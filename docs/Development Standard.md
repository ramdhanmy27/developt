#Development Standard

#1. Struktur File

```javascript
.
|-- app
|    |-- Apps
|    |-- Console
|    |-- Events
|    |-- Exceptions
|    |-- Facades
|    |-- Http
|    |    |-- Controllers
|    |    |-- Helpers
|    |    |-- Middleware
|    |    |-- Requests
|    |-- Jobs
|    |-- Libraries
|    |-- Listeners
|    |-- Models
|    |    |-- Interfaces
|    |-- Policies
|    |-- Providers
|    |-- Services
|-- bootstrap
|-- config
|-- database
|    |-- factories
|    |-- migrations
|    |-- seeds
|-- public
|    |-- css
|    |-- fonts
|    |-- img
|    |-- js
|    |-- theme
|-- resources
|    |-- assets
|    |    |-- css
|    |    |-- js
|    |    |-- less
|    |-- lang
|    |-- theme
|    |-- views
|-- storage
|-- tests
|-- vendor
```

#2. Coding Standard
##2.1. Penggunaan Case
###Studly Caps
Gunakan huruf besar pada setiap awal kata. Dua kata atau lebih disambung tanpa menggunakan spasi.

- Namespace dan Class
- Folder yang digunakan sebagai namespace
- Nama aplikasi
 
> contoh: `App\Models\HumanResource`

###Camel Case
Hampir sama seperti Pascal Case, hanya saja huruf paling awal menggunakan huruf kecil.

- Nama method (fungsi class) 
 
> contoh: `public function clearCache()`

###Snake Case
Selalu gunakan huruf kecil. Dua kata atau lebih disambung menggunakan underscore.

- Fungsi helper
- Variable / property
- nama Database, table, field
 
> contoh: `function contoh_fungsi()`

###Hyphen
Selalu gunakan huruf kecil. Dua kata atau lebih disambung menggunakan strip / hyphen `-`.

- url
- folder / file view
- javascript, css, image, less/sass
- css class
- html attribute, tag
 
> contoh: `bootstrap-datepicker.js`

##2.2. Penggunaan library
  - pdf : barryvdh/laravel-dompdf
  - image : intervention/image
  - table : datatable (yajra/laravel-datatables-oracle)
  - chart : khill/lavacharts (Google Chart API)

##2.3. Komentar
Beberapa programmer kadang merasa malas untuk menambahkan komentar pada program yang dibuatnya. Akan tetapi penggunaan komentar pada source code sangat dibutuhkan, apalagi pada source yang sangat panjang. 

Dalam menuliskan komentar, anda tidak perlu menambahkannya pada setiap line baris kode. Cukup pada bagian yang dirasa perlu diberi keterangan saja.

Contoh:

```php
$isZip = false;

// build path for requested files
$requestedFiles = $req->input("path", []);
$requestedFiles = array_filter(array_map(function($item) use (&$isZip) {
  if (!Storage::exists($item))
    return;

  $requestedFiles = Drive::path($item);

  // will create archive file if path is folder
  if (is_dir($requestedFiles))
    $isZip = true;

  return $requestedFiles;
}, is_array($requestedFiles) ? $requestedFiles : [$requestedFiles]));

$filesCount = count($requestedFiles);
$isZip = $isZip || $filesCount > 1;

// invalid download request
if ($filesCount == 0) 
  throw new ServiceException("Request failed.");

// return zipped file if multiple
if ($isZip) {
  $file = date("YmdHis-").preg_replace("/[\.\s]/", "", microtime()).rand().".zip";
  $filepath = Drive::path($file, "tmp");

  // create archive file
  Zippy::load()->create($filepath, $requestedFiles);

  // get file content and make download response
  $response = response(file_get_contents($filepath))
          ->header("Content-Length", filesize($filepath));

  // delete zip file
  Storage::disk("tmp")->delete($file);

  // return download response
  return $response
      ->header("Content-Description", "File Transfer")
      ->header("Content-Type", "application/octet-stream")
      ->header("Content-Transfer-Encoding", "binary")
      ->header("Expires", "0")
      ->header("Cache-Control", "must-revalidate")
      ->header("Content-Disposition", "attachment; filename=document-".date("Y-m-d").".zip");
}

// return single file to download
return response()->download(current($requestedFiles));
```

###Fungsi & Method
Selalu gunakan komentar pada setiap definisi fungsi. Jelaskan kegunaan fungsi tersebut, tipe data parameter beserta outputnya. 

Contoh:

```php
/**
 * Convert angka ke format currency
 * 
 * @param  integer  $value     
 * @param  integer  $decimals  jumlah angka belakang koma
 * @param  string   $currency  format currency
 * @return string
 */
function currency($value, $decimals=0, $currency="Rp") 
{
    return "$currency ".number_format($value, $decimals);
}
```

##2.4. Coding Style
Coding style aplikasi mengikuti Coding Standard PSR-1 dan PSR-2. Silahkan buka halaman berikut.

http://www.php-fig.org/psr/psr-1/
http://www.php-fig.org/psr/psr-2/

#3. Penempatan Source
Penempatan source code terbagi atas 2 macam. Ada yang bersifat **modular** dan **global**.

##3.1. Modular

```javascript
app
 |-- Apps
      |-- NamaAplikasi
           |-- assets // Resource asset aplikasi (Javascript, CSS, Images, Font, dll)
           |-- Controllers // Controllers
           |-- Middleware // Middleware
           |-- Facades // Facades
           |-- Helpers // Fungsi tanpa class / namespace
           |-- Models // Model aplikasi
           |-- Requests // Validasi request
           |-- Services // Custom library
           |-- views // view aplikasi
           |-- / definitions.php / // Definisi aplikasi, fitur, credential
           |-- / routes.php / // Route aplikasi
           |-- / gulpfile.js / // custom gulp khusus aplikasi #optional
```
Dalam satu project terdapat beberapa aplikasi yang dipisah secara modular. Modul aplikasi ini terdiri atas beberapa folder. Di setiap folder root aplikasi diwajibkan ada file berikut :

- `definitions.php` 
  Berguna sebagai pendefinisian aplikasi beserta fitur dan credential-nya.
- `routes.php`
  Digunakan untuk mendefinisikan route aplikasi.

##3.2. Global

```javascript
app
 |-- Facades
 |-- Http
 |    |-- Controllers
 |    |-- Helpers // Kumpulan fungsi
 |    |-- Middleware
 |    |-- Requests
 |    |-- / routes.php / // Route aplikasi
 |-- Models
 |    |-- Interfaces
 |-- public // Resource asset
 |-- resources
 |    |-- views
 |-- Services // Custom library
```

##3.3. Assets

Penempatan file asset terbagi menjadi dua bagian, `Development` dan `Distribution`. Jika anda sudah pernah menggunakan laravel, sebagai perumpamaan anda bisa menganggap `development` adalah semua file asset yang ada di dalam folder `resources/assets`. Lalu `distribution` adalah semua file asset yang ada didalam folder `public`.

###3.3.1. Framework Assets

Pada framework ini sudah disediakan file javascript & css untuk mempermudah pembuatan aplikasi. Jika ingin merubahnya atau menambahkan beberapa fungsi, pertama-tama install `gulp` dan `laravel-elixir`. Tata cara menginstallnya bisa anda lihat disini https://laravel.com/docs/elixir.

####Javascript

Ada empat file javascript yang sudah disiapkan untuk framework ini. Untuk melakukan perubahan, tambahkan script pada file berikut.

- Jika ingin menambahkan fungsi sederhana yang bersifat seperti helper, tambahkan di file `/resources/assets/js/fn.js`
- Event yang bersifat general (seperti modal atau message alert) tambahkan di file `/resources/assets/js/event.js`
- Apabila ingin menambahkan service, filter atau factory untuk script angular, tambahkan di file `/resources/assets/js/angular.init.js`
- Script global jQuery tambahkan di file `/resources/assets/js/init.js`

####CSS

Untuk meng-custom css framework ini, anda harus menggunakan LESS. Pada framework ini sudah disediakan beberapa file sebagai berikut.

- `all.less` - file yang dipanggil saat dilakukan kompilasi LESS
- `app.less` - taruh script general yang mempengaruhi framework keseluruhan disini
- `common.less` - script helper class. Digunakan di semua file LESS lainnya
- `theme-override.less` - Jika anda ingin menata ulang template tanpa merubah filenya, gunakan file 

###3.3.2. Application Assets

Penempatan file asset memiliki pola yang sama, yang membedakan adalah path dari `assetDir`-nya saja. Secara garis besar dapat digambarkan seperti ini.

> __Javascript :__ __[assetDir]__/js
> __CSS :__ __[assetDir]__/css
> __LESS :__ __[assetDir]__/less
> __Image :__ __[assetDir]__/img
> __Font :__ __[assetDir]__/fonts

Khusus untuk css dan javascript, didalam foldernya (`[assetDir]/[js/css]`) terpisah lagi bedasarkan penggunaannya. 

 1. Controller 

 Folder untuk menyimpan file yang dikhususkan untuk satu Controller.
 > __[dir]__/app/__[controller]__

 2. Library
 Folder untuk menyimpan file Library yang dikhususkan untuk satu aplikasi.
 > __[dir]__/lib

 3. Shared
 Jika file tidak dikhususkan untuk satu Controller, cukup taruh di dalam folder `[dir]`.

>__Note :__ [dir] => [assetDir]/[js/css]

####a. Global

Path `assetDir` untuk pola yang global mengarah ke folder `public`.

####b. Modular

Path `assetDir` untuk pola yang modular mengarah ke folder asset aplikasinya (`app/Apps/[aplikasi]/assets`). Hal ini sangat diperlukan agar semua hal yang berhubungan dengan aplikasi dapat dikapsulasi. Namun seperti yang kita ketahui pada framework laravel, semua asset seperti javascript, css, gambar, dll. harus ditempatkan ke dalam folder `public` agar mudah diakses. Untuk menyiasati hal tersebut, kita bisa meng-copy semua file yang ada di dalam folder `assets` aplikasi ke dalam folder `public/app/[aplikasi]`. Tentunya itu tidak praktis dan tidak direkomendasikan. Atau satu lagi kita bisa menggunakan tool `gulp` sebagai alat bantu. Cukup ketikkan `gulp watch` pada console, lalu setiap perubahan yang ada pada aplikasi akan di update secara otomatis. 

Secara default, semua asset yang ada pada aplikasi akan diduplikasi ke dalam folder public. Akan tetapi, jika anda sudah mengenal baik `Elixir` yang ada pada laravel, anda bisa menentukan sendiri apa yang akan didistribusikan ke dalam folder public. 

Pada framework ini anda bisa membuat gulpfile khusus hanya untuk satu aplikasi saja. Caranya cukup buat file `gulpfile.js` dan taruh ke dalam folder aplikasi (`app/Apps/[aplikasi]`). Dari sini anda bisa meng-koding `elixir` seperti biasanya. Atau anda juga bisa gunakan module `modulixir` sebagai pengganti `elixir`.

> __Note : __ Untuk mengetahui lebih detail tentang modulixir, anda bisa melihatnya di Dokumentasi

```javascript
// gulpfile.js

var mlixir = require("../modulixir");

mlixir(__dirname, function(mix) {
  mix
    .styles([
      "style.css" // app/Apps/[aplikasi]/assets/css/style.css
    ], mlixir.path.css+ "merge.css")    
    // mlixir.path.css -> public/app/[aplikasi]/css
  
    .scripts([
      "script.js" // app/Apps/[aplikasi]/assets/js/script.js
    ], mlixir.path.js+ "merge.js");
    // mlixir.path.css -> public/app/[aplikasi]/js
})
``` 

###3.3.3. Theme Assets

Fitur tema ini adalah hasil kostumisasi dari framework laravel. Pada framework ini kita dapat mendefinisikan lebih dari satu tema. Untuk melakukannya buat folder baru `resources/theme/[nama-tema]/`. Buat file `gulpfile.js` didalam folder tema  jika perlu. Buat juga folder `views` untuk menyimpan file view. Lalu buat folder `assets` untuk menyimpan file asset tema. Folder ini yang akan digunakan sebagai `assetDir`.

```javascript
.
|-- resources/theme/[nama-tema]
|    |-- views // view aplikasi
|    |-- assets // assetDir
|    |-- / gulpfile.js /
```

#4. Penggunaan Source
##Javascript & CSS

Pada template blade framework disediakan 2 stack khusus untuk menambahkan script css dan javascript.

###App Stack

Jika anda ingin membuat script khusus yang digunakan untuk beberapa halaman saja, stack ini adalah pilihan yang tepat. Pada stack ini, anda bisa meng-override fungsi-fungsi yang sudah ada ataupun membuat fungsi baru dengan fungsi-fungsi yang sudah disediakan. Gunakan stack `app-script` untuk menambahkan script javascript. dan gunakan stack `app-style` untuk menambahkan script css.

```html
@push("app-style")
  <link rel="stylesheet" href="{{ asset('css/app/hr/pegawai/style.css') }}" />
@endpush

@push("app-script")
  <script src="{{ asset('js/app/hr/pegawai/form.js') }}"></script>
@endpush
```

###Dependency Stack

Posisi stack ini berada diantara script dependency wajib dan script bawaan framework. Oleh karena itu, stack ini dapat digunakan untuk menambahkan script css/javascript yang bersifat sebagai library/dependency tambahan.

```html
@push("style")
  <link rel="stylesheet" href="{{ asset('css/app/lib/dropzone.css') }}" />
@endpush

@push("script")
  <script src="{{ asset('js/app/lib/dropzone.js') }}"></script>
@endpush
```

#5. Pembuatan UI
##Standard UI

- front page aplikasi
- list view data master
  - Table harus menggunakan datatable
  - Default sort bedasarkan label (seperti: nama, Judul)
- editor (create / edit) data master; bisa single atau multiple dataset
- viewer data master
- list view data transaksi 
  - Table harus menggunakan datatable
  - Default sort bedasarkan time
  - Class Model harus mengimplementasi interface ```App\Models\Interfaces\TransactionModel```
- input / edit transaksi
- view transaksi
- slip, yaitu output berupa cetakan sebagai bukti legal dari suatu transaksi
  - Generate Pdf harus menggunakan barryvdh/laravel-dompdf
- report, yaitu hasil query, pengolahan, dan layout dari berbagai data
  - Generate Pdf harus menggunakan barryvdh/laravel-dompdf
- Dashboard
  - Pembuatan chart harus menggunakan khill/lavacharts atau Google Chart

##Generate PDF

- harus menggunakan blade sebagai template layout pdf
- layout blade harus merupakan turunan dari report.layout
- Generate Pdf harus menggunakan **barryvdh/laravel-dompdf**
- Pembuatan chart harus menggunakan **khill/lavacharts** atau **Google Chart**
