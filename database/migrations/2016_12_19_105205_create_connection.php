<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("connection", function(Blueprint $table) {
            $table->increments("id");
            $table->string("driver");
            $table->string("name");
            $table->string("username")->nullable();
            $table->string("password")->nullable();
            $table->string("prefix", 10)->nullable();
            $table->text("config")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("connection");
    }
}
