<?php

use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;

class TeritorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function seedFromCSV($file,$table,$delimiter = ","){
        $file = fopen(base_path('storage/app/seeds/'.$file),"r");
        $loop = false;
        while(! feof($file))
        {
            $csv = fgetcsv($file,999999,$delimiter);
            if(!$loop){
                $column = $csv;
                $loop = true;
            }else{
                for($i=0;$i<count($csv);$i++){
                    $data[$column[$i]] = utf8_encode($csv[$i]);
                }
                $table::updateOrCreate($data);
            } 
        }
        fclose($file);
    }

    public function run()
    {
    	$this->seedFromCSV('country.csv', Country::class);
    	$this->seedFromCSV('provinces.csv', Province::class);
    	$this->seedFromCSV('regencies.csv', Regency::class);
    	$this->seedFromCSV('districts.csv', District::class);
    	$this->seedFromCSV('villages.csv', Village::class);
    }
}
