<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function seedFromCSV($file,$table,$delimiter = ","){
        $file = fopen(base_path('storage/app/seeds/'.$file),"r");
        $loop = false;
        while(! feof($file))
        {
            $csv = fgetcsv($file,999999,$delimiter);
            if(!$loop){
                $column = $csv;
                $loop = true;
            }else{
                for($i=0;$i<count($csv);$i++){
                    $data[$column[$i]] = (utf8_encode($csv[$i]))?:NULL;
                }
                $table::updateOrCreate($data);
            } 
        }
        fclose($file);
    }

    public function run()
    {
    	$this->seedFromCSV('currency.csv', Currency::class);
    }
}
